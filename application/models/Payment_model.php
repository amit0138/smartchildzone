<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model
{

    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function classesPaymentCount($searchText = '',$email)
    {
        $this->db->select('*');
        $this->db->from('sc_payments as BaseTbl');
        
        $this->db->where('payer_email', $email);
        $query = $this->db->get();
        return count($query->result());
    }

    /**
     * This function used to get Payment list by email
     * @param number $email : This is user email
     * @return array $result : This is payment list
     */
   
    function paymentListing($searchText = '', $page, $segment,$email)
    {
        //print_r($segment);exit;
        $this->db->select('*');
        $this->db->from('sc_payments as BaseTbl');
       
        $this->db->where('payer_email', $email);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
}

  