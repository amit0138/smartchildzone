<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Month_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function monthListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.name');
        $this->db->from('tbl_months as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function monthListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.name');
        $this->db->from('tbl_months as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    

    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewMonth($monthInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_months', $monthInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get Subject information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getMonthInfo($subjectid)
    {
        $this->db->select('id, name');
        $this->db->from('tbl_months');
        $this->db->where('isDeleted', 0);
        $this->db->where('id', $subjectid);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editMonth($subjectInfo, $subjectId)
    {
        $this->db->where('id', $subjectId);
        $this->db->update('tbl_months', $subjectInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteMonth($subjectId, $subjectInfo)
    {
        $this->db->where('id', $subjectId);
        $this->db->update('tbl_months', $subjectInfo);
        
        return $this->db->affected_rows();
    }

    function getAllMonth(){
        $this->db->select('id,name');
        $this->db->from('tbl_months');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }

    public function monthAlldata(){
        $this->db->select('*');
        $this->db->from('tbl_months as BaseTbl');
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
}

  