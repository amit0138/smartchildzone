<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function ListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('sc_user_registration as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function Listing($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('sc_user_registration as BaseTbl');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    

    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewCoupans($coupanInfo)
    {
        $this->db->trans_start();
        $this->db->insert('sc_master_coupons', $coupanInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get Subject information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getCoupanInfo($coupanId)
    {
        $this->db->select('*');
        $this->db->from('sc_master_coupons');
        $this->db->where('isDeleted', 0);
        $this->db->where('c_id', $coupanId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editCoupan($coupanInfo, $coupanId)
    {
        $this->db->where('c_id ', $coupanId);
        $this->db->update('sc_master_coupons', $coupanInfo);
        
        return TRUE;
    }

    function getAllCoupans(){
        $this->db->select('*');
        $this->db->from('sc_master_coupons');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }

    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteCoupan($coupanId, $coupanInfo)
    {
        $this->db->where('c_id', $coupanId);
        $this->db->update('sc_master_coupons', $coupanInfo);
        
        return $this->db->affected_rows();
    }


}

  