<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Days_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function daysListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.name');
        $this->db->from('tbl_days as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function daysListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.name, Month.name as month, Sub.name as subject,Class.name as class');
        $this->db->join('tbl_months as Month', 'Month.id = BaseTbl.month_id','left');
        $this->db->join('tbl_subject as Sub', 'Sub.id = BaseTbl.sub_id','left');
        $this->db->join('tbl_classes as Class', 'Class.id = BaseTbl.class_id','left');
        $this->db->from('tbl_days as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    

    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewDay($dayInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_days', $dayInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get Subject information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getDayInfo($dayid)
    {
        $this->db->select('*');
        $this->db->from('tbl_days');
        $this->db->where('isDeleted', 0);
        $this->db->where('id', $dayid);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editDay($dayInfo, $dayId)
    {
        $this->db->where('id', $dayId);
        $this->db->update('tbl_days', $dayInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteDay($dayId, $dayInfo)
    {
        $this->db->where('id', $subjectId);
        $this->db->update('tbl_days', $dayInfo);
        
        return $this->db->affected_rows();
    }

    public function dayAlldata(){
        $this->db->select('BaseTbl.id, BaseTbl.name, Month.name as month, Sub.name as subject,Class.name as class');
        $this->db->join('tbl_months as Month', 'Month.id = BaseTbl.month_id','left');
        $this->db->join('tbl_subject as Sub', 'Sub.id = BaseTbl.sub_id','left');
        $this->db->join('tbl_classes as Class', 'Class.id = BaseTbl.class_id','left');
        $this->db->from('tbl_days as BaseTbl');
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
}

  