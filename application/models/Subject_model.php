<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Subject_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function subjectListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_subject as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function subjectListing($searchText = '', $page, $segment)
    {
       // print_r($page); exit;
        $this->db->select('BaseTbl.id, BaseTbl.name,BaseTbl.month_id,Month.name as month');
        $this->db->join('tbl_months as Month', 'Month.id = BaseTbl.month_id','left');
        $this->db->from('tbl_subject as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    

    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewSubject($subjectInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_subject', $subjectInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get Subject information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getSubjectInfo($subjectid)
    {
        $this->db->select('*');
        $this->db->from('tbl_subject');
        $this->db->where('isDeleted', 0);
        $this->db->where('id', $subjectid);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editSubject($subjectInfo, $subjectId)
    {
        $this->db->where('id', $subjectId);
        $this->db->update('tbl_subject', $subjectInfo);
        
        return TRUE;
    }

    function getAllSubject(){
        $this->db->select('id,name');
        $this->db->from('tbl_subject');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }

    function getAllClasses(){
        $this->db->select('id,name');
        $this->db->from('tbl_classes');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }

    function getAllDays(){
        $this->db->select('id,name');
        $this->db->from('tbl_days');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }

    function getAllTopic(){
        $this->db->select('id,name');
        $this->db->from('tbl_topic');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }
    

    function getMonthBySubject($month_id)
    {
        $this->db->select('id,name');
        $this->db->from('tbl_subject');
        $this->db->where('month_id', $month_id);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSubject($subjectId, $subjectInfo)
    {
        $this->db->where('id', $subjectId);
        $this->db->update('tbl_subject', $subjectInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId, password');
        $this->db->where('userId', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }

    public function subjectAlldata(){
        $this->db->select('*');
        $this->db->from('tbl_subject as BaseTbl');
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
}

  