<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class commission_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function commissionListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_commission as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.commission  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function commissionListing($searchText = '', $page, $segment)
    {
        $this->db->select('Coupan.c_coupon_code as coupan_code,BaseTbl.commission,BaseTbl.id');
        $this->db->join('sc_master_coupons as Coupan', 'Coupan.c_id = BaseTbl.coupan_id','left');
        $this->db->from('tbl_commission as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.commission  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    

    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewcommission($commissionInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_commission', $commissionInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get Subject information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getCommissionInfo($commissionid)
    {
        $this->db->select('*');
        $this->db->from('tbl_commission');
        $this->db->where('isDeleted', 0);
        $this->db->where('id', $commissionid);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editCommission($c_Info, $commissionid)
    {
        $this->db->where('id ', $commissionid);
        $this->db->update('tbl_commission', $c_Info);
        
        return TRUE;
    }

    function getAllCommission(){
        $this->db->select('*');
        $this->db->from('tbl_commission');
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();

        return $query->result();
    }

    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteCoupan($coupanId, $coupanInfo)
    {
        $this->db->where('c_id', $coupanId);
        $this->db->update('sc_master_coupons', $coupanInfo);
        
        return $this->db->affected_rows();
    }


}

  