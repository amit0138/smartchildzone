<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = 'error';

/****************Pages For ROUTES ***********************/

$route['learning-plan'] = 'home/subject';
$route['learning-plan/(:any)'] = 'home/subject/$1';
$route['learning-plan/(:any)/(:any)'] = 'home/subject/$1/$1';
$route['learning-plan/(:any)/(:any)/(:any)'] = 'home/subject/$1/$1/$1';
$route['learning-plan/(:any)/(:any)/(:any)/(:any)'] = 'home/subject/$1/$1/$1/$1';
$route['learning-plan/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'home/subject/$1/$1/$1/$1/$1';



/*********** USER DEFINED ROUTES *******************/
//Admin Login
$route['loginMe'] = 'admin/login';

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';
$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";

$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";
$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";

//Route For Subject Label
$route['addNewSubject'] = "subject/addNewSubject";
$route['subjectListing'] = "subject/subjectListing";
$route['storeSubject'] = "subject/storeSubject";
$route['subjectEdit/(:num)'] = "subject/subjectEdit/$1";
$route['subjectListing/(:num)'] = "user/subjectListing/$1";
$route['updateSubject'] = "subject/updateSubject";
$route['deleteSubject'] = "subject/deleteSubject";

//Route For Classes Label
$route['addNewClasses'] = "classes/addNewClasses";
$route['classesListing'] = "classes/classesListing";
$route['storeClasses'] = "classes/storeClasses";
$route['classesEdit/(:num)'] = "classes/classesEdit/$1";
$route['updateClasses'] = "classes/updateClasses";
$route['deleteClasses'] = "classes/deleteClasses";

//Route For Months
$route['addNewMonth'] = "month/addNewMonth";
$route['MonthListing/(:num)'] = "month/MonthListing/$1";
$route['storeMonth'] = "month/storeMonth";
$route['monthsEdit/(:id)'] = "month/monthsEdit/$1";
$route['updateMonth'] = "month/updateMonth";
$route['deleteMonth'] = "month/deleteMonth";

$route['getMonthBySubject'] = "classes/getMonthBySubject";
$route['getSubjectByClasses'] = "classes/getSubjectByClasses";
$route['getDaysByClasses'] = "classes/getDaysByClasses";
$route['getTopicByDay'] = "classes/getTopicByDay";
$route['getTopicByClass'] = "classes/getTopicByClass";

$route['updateSlugByTitle/(:any)'] = "classes/updateSlugByTitle";




//Route For Days
$route['addNewday'] = "days/addNewday";
$route['DaysListing'] = "days/DaysListing";
$route['storeDays'] = "days/storeDays";
$route['daysEdit/(:id)'] = "days/daysEdit/$1";
$route['updateDay'] = "days/updateDay";
$route['deleteDay'] = "days/deleteDay";

//Route For Topic
$route['addNewTopic'] = "topic/addNewTopic";
$route['topicsListing'] = "topic/topicsListing";
$route['storeTopic'] = "topic/storeTopic";
$route['topicEdit/(:id)'] = "topic/TopicEdit/$1";
$route['updateTopic'] = "topic/updateTopic";
$route['deleteTopic'] = "topic/deleteTopic";

//Route For Subtopic
$route['addNewSubtopic'] = "subtopic/addNewSubtopic";
$route['SubtopicListing'] = "subtopic/SubtopicListing";
$route['storeSubtopic'] = "subtopic/storeSubtopic";
$route['subtopicEdit/(:id)'] = "subtopic/subtopicEdit/$1";
$route['updateSubtopic'] = "subtopic/updateSubtopic";
$route['deleteSubtopic'] = "subtopic/deleteSubtopic";

//Route For Coupns
$route['addNewCoupans'] = "coupans/addNewCoupans";
$route['coupansListing'] = "coupans/coupansListing";
$route['storeCoupans'] = "coupans/storeCoupans";
$route['coupansEdit/(:id)'] = "coupans/coupansEdit/$1";
$route['updateCoupans'] = "coupans/updateCoupans";
$route['deleteCoupans'] = "coupans/deleteCoupans";

//Coupan Assign Listing
$route['assignListing'] = "coupans/assignListing";
$route['addNewCoupansassign'] = "coupans/addNewCoupansassign";
$route['storeCoupansassign'] = "coupans/storeCoupansassign";
$route['coupansassignEdit/(:id)'] = "coupans/coupansassignEdit/$1";
$route['updateCoupansassign'] = "coupans/updateCoupansassign";
$route['deletecoupansassign'] = "coupans/deletecoupansassign";
//Route For Coupns Assign



//Route For Commision Assign
$route['addNewcommission'] = "commission/addNewcommission";
$route['commissionListing'] = "commission/commissionListing";
$route['storecommission'] = "commission/storecommission";
$route['commissionEdit/(:id)'] = "commission/commissionEdit/$1";
$route['updateCommission'] = "commission/updateCommission";
$route['deletecommission'] = "commission/deletecommission";

//Route For Marketrs
$route['addnewmarketers'] = "marketers/addnewmarketers";
$route['marketersListing'] = "marketers/marketersListing";
$route['storeMarketers'] = "marketers/storeMarketers";
$route['editMarketers/(:id)'] = "marketers/editMarketers/$1";
$route['updateMarketers'] = "marketers/updateMarketers";
$route['deleteMarketrs'] = "marketers/deleteMarketrs";

//Route For Payment
$route['paymentListing'] = "payment/paymentListing";
$route['getPaymentList'] = "payment/getPaymentList";

//Route For Video
$route['videoListing'] = "video/videoListing";
$route['addNewVideo'] = "video/addNewVideo";
$route['storeVideo'] = "video/video/storeVideo";

//Customer Route
$route['Listing'] = "customers/listing";




/* End of file routes.php */
/* Location: ./application/config/routes.php */