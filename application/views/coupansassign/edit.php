<?php

$assignid = '';
$coupanid = '';
$marid = '';

if(!empty($coupansassignInfo))
{
    foreach ($coupansassignInfo as $sub)
    {
        $assignid = $sub->id;
        $coupanid = $sub->coupan_id;
        $marid = $sub->mar_id;
    }
}

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Coupan Assign Management
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Coupan Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>coupans/updateCoupansassign" method="post" id="editsubject" role="form">
                        <div class="box-body">
                            <div class="row">
                                <input type="hidden" value="<?php echo $assignid; ?>" name="assignId" id="coupanid" />    
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Coupan</label>
                                        <select name="coupon_id" id="coupan" class="form-control required" required>
                                            <option value="">Select Coupan</option>
                                             
                                            <?php foreach ($coupans as  $value) { 
                                                $selectedcoupan = $value->c_id==$coupanid?'selected':'';
                                            ?>
                                            <option value="<?=$value->c_id ?>" <?= $selectedcoupan ?> ><?php echo $value->c_coupon_code;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Markters</label>
                                        <select name="marketer_id" id="marketer" class="form-control required" required>
                                            <option value="">Select Marketer</option>
                                            <?php foreach ($marketers as  $value) { 
                                              $selectedmarkters = $value->userId==$marid?'selected':'';    
                                            ?>
                                            <option value="<?=$value->userId ?>" <?= $selectedmarkters ?>><?php echo $value->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                               
                               
                                </div>
                            </div>
                         
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>
