<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-plane"></i> Customers Management
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <!-- <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>classes/addNewClasses"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div> -->
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Customer List</h3>
                    <div class="box-tools">
                       
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>User Id</th>
                      <th>User Name</th>
                      <th>Email</th>
                      <th>Status</th>
                      <th>Type</th>
                      <th>Membership Id</th>
                      <th>Created At</th>
                      <th>Country</th>
                      <th>State</th>
                      <th>Phone</th>
                      <th>No. of Child</th>
                      <th>Pack Price</th>
                      <th>Subscription</th>
                      <th>Subscription Date</th>
                    </tr>
                    <?php
                    if(!empty($customers))
                    {
                      $total = 0;
                    foreach($customers as $record)
                    {
                    ?>
                    <tr>
                      <td><?php echo $record->user_id ?></td>
                      <td><?php echo $record->user_name ?></td>
                      <td><?php echo $record->user_email ?></td>
                      <td><?php echo $record->user_status ?></td>
                      <td><?php echo $record->user_type ?></td>
                      <td><?php echo $record->class_membership_id ?></td>
                      <td><?php echo $record->created_at ?></td>
                      <td><?php echo $record->user_country ?></td>
                      <td><?php echo $record->user_state ?></td>
                      <td><?php echo $record->user_phone ?></td>
                      <td><?php echo $record->c_no_of_child ?></td>
                      <td><?php echo $record->c_pack_price ?></td>
                      <td><?php echo $record->c_subscription ?></td>
                      <td><?php echo $record->c_subscribe_date ?></td>
                    </tr>
                    <?php
                    $total +=$record->c_pack_price;
                        }
                    ?>
                    <tr>
                     <td colspan="11" class="text-right"> <b>Total Pack Price</b></td>
                     <td>$<?= $total ?></td>
                      </tr>  
                    <?php }else{ ?>
                      <tr class="text-center text-danger">
                      
                       <td Colspan="3">No Record Found</td>
                      </tr>
                    
                    <?php }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "subjectListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
