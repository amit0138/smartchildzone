<?php

$topicId = '';
$name = '';

if(!empty($topicInfo))
{
    foreach ($topicInfo as $topic)
    {
        $topicId = $topic->id;
        $name = $topic->name;
        $class_id = $topic->class_id;
        $month_id = $topic->month_id;
        $subject_id = $topic->subject_id;
        $day_id = $topic->day_id;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Topic Management
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Topic Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>topic/updateTopic" method="post" id="editsubject" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Topic Name</label>
                                        <input type="text" class="form-control" id="name" placeholder="Subject Title" name="name" value="<?php echo $name; ?>" maxlength="128" required>
                                        <input type="hidden" value="<?php echo $topicId; ?>" name="topicId" id="subjectId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Month</label>
                                        <select id="month" class="form-control required" name="month">
                                            <option value="">Select Month</option>
                                            <?php foreach ($months as  $month) {
                                                $selectedMonth = $month->id==$month_id?'selected':'';
                                             ?>
                                            <option value="<?= $month->id ?>" <?= $selectedMonth ?>><?php echo $month->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="subject">Subject</label>
                                        <select class="form-control required" name="subject" id="subject" required>
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subjects as  $value) { 
                                                $selectedSubject = $value->id==$subject_id?'selected':'';
                                            ?>
                                            <option value="<?=$value->id?>" <?= $selectedSubject ?> ><?php echo $value->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="class">Classes</label>
                                        <select id="class" name="class" class="form-control required">
                                            <option>Select Class</option>
                                            <?php foreach ($classes as  $value) { 
                                                $selectedClass = $value->id==$class_id?'selected':'';
                                            ?>
                                            <option value="<?=$value->id?>" <?= $selectedClass ?> ><?php echo $value->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="days">Days</label>
                                        <select name="days" id="days" class="form-control required">
                                            <option>Select Days</option>
                                            <?php foreach ($days as  $day) {
                                                $selectedDay = $day->id==$day_id?'selected':'';
                                             ?>
                                            <option value="<?= $day->id ?>" <?= $selectedDay ?>><?php echo $day->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                            </div>
                         
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>
<script type="text/javascript">
    $(document).on('change','#month',function(e){
        var option_val = $('#month').val();
           
            var html_option = '<option value="">Select</option>';

            if(option_val !=''){
                $('#preloader').removeClass('hide');
                requestcallbackurl = baseURL+'getMonthBySubject';
                $.ajax({
                    type : "POST",
                    dataType : "json",
                    url : requestcallbackurl,
                    data : { id : option_val } 
                }).then(function(response) {
                    console.log(response);
                   
                    $.each(response, function(index, item) {
                        if(item.id !=''){
                            html_option = html_option+'<option value="'+item.id+'">'+item.name+'</option>'; 
                        }
                    });

                    $('#subject').html(html_option);
                    $('#preloader').addClass('hide');
                });
            }
    });

    $(document).on('change','#subject',function(e){
        var option_val = $('#subject').val();
           
            var html_option = '<option value="">Select</option>';

            if(option_val !=''){
                $('#preloader').removeClass('hide');
                requestcallbackurl = baseURL+'getSubjectByClasses';
                $.ajax({
                    type : "POST",
                    dataType : "json",
                    url : requestcallbackurl,
                    data : { id : option_val } 
                }).then(function(response) {
                    console.log(response);
                   
                    $.each(response, function(index, item) {
                        if(item.id !=''){
                            html_option = html_option+'<option value="'+item.id+'">'+item.name+'</option>'; 
                        }
                    });

                    $('#class').html(html_option);
                    $('#preloader').addClass('hide');
                });
            }
    });

    $(document).on('change','#class',function(e){
        var option_val = $('#class').val();
           
            var html_option = '<option value="">Select</option>';

            if(option_val !=''){
                $('#preloader').removeClass('hide');
                requestcallbackurl = baseURL+'getDaysByClasses';
                $.ajax({
                    type : "POST",
                    dataType : "json",
                    url : requestcallbackurl,
                    data : { id : option_val } 
                }).then(function(response) {
                    console.log(response);
                   
                    $.each(response, function(index, item) {
                        if(item.id !=''){
                            html_option = html_option+'<option value="'+item.id+'">Day '+item.name+'</option>'; 
                        }
                    });

                    $('#days').html(html_option);
                    $('#preloader').addClass('hide');
                });
            }
    });
</script>