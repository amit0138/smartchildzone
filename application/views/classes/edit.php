<?php

$subjectId = '';
$name = '';

if(!empty($classInfo))
{
    foreach ($classInfo as $class)
    {
        $class_id = $class->id;
        $name = $class->name;
        $month = $class->month_id;
        $subject = $class->subject_id;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Classes Management
        <small>Add / Edit Class</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Class Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>classes/updateClasses" method="post" id="editsubject" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Subject Title</label>
                                        <input type="text" class="form-control" id="name" placeholder="Subject Title" name="name" value="<?php echo $name; ?>" maxlength="128" required>
                                        <input type="hidden" value="<?php echo $class_id; ?>" name="class_id" id="class_id" />    
                                    </div>
                                    
                                </div>

                                 <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Month</label>
                                        <select name="month" id="month" class="form-control required">
                                            <option>Select Month</option>
                                            <?php foreach ($months as  $value) { 
                                                $selectedMonth = $value->id==$month?'selected':'';
                                                ?>
                                            <option value="<?=$value->id?>" <?= $selectedMonth ?> ><?php echo $value->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="subject">Subject</label>
                                        <select id="subject" name="subject" class="form-control required">
                                            <option>Select Subject</option>
                                            <?php foreach ($subjects as  $value) { 
                                                $selectedSubject = $value->id==$subject?'selected':'';
                                            ?>
                                            <option value="<?=$value->id?>" <?= $selectedSubject ?> ><?php echo $value->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                         
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).on('change','#month',function(e){
        var option_val = $('#month').val();
           
            var html_option = '<option value="">Select</option>';

            if(option_val !=''){

                requestcallbackurl = baseURL+'getMonthBySubject';
                $.ajax({
                    type : "POST",
                    dataType : "json",
                    url : requestcallbackurl,
                    data : { id : option_val } 
                }).then(function(response) {
                    console.log(response);
                   
                    $.each(response, function(index, item) {
                        if(item.id !=''){
                            html_option = html_option+'<option value="'+item.id+'">'+item.name+'</option>'; 
                        }
                    });

                    $('#subject').html(html_option);

                });
            }
    });
</script>