<?php

$coupanid = '';
$coupan_code = '';
$coupan_per = '';
$coupan_des = '';
$coupan_status = '';

if(!empty($coupansInfo))
{
    foreach ($coupansInfo as $sub)
    {
        $coupanid = $sub->c_id;
        $coupan_code = $sub->c_coupon_code;
        $coupan_per = $sub->c_coupon_per;
        $coupan_des = $sub->c_coupon_desc;
        $coupan_status = $sub->c_coupon_status;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Coupan Management
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Coupan Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>coupans/updateCoupans" method="post" id="editsubject" role="form">
                        <div class="box-body">
                            <div class="row">
                                <input type="hidden" value="<?php echo $coupanid; ?>" name="coupansId" id="coupanid" />    
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="coupan_code">Coupan Code</label>
                                        <input type="text" class="form-control required" placeholder="Coupan Code" id="name" value="<?php echo $coupan_code; ?>" name="c_coupon_code" maxlength="128">
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="c_coupon_per">Coupan Per</label>
                                        <input type="text" class="form-control required" placeholder="Coupan Per" id="name" value="<?php echo $coupan_per; ?>" name="c_coupon_per" maxlength="128">
                                    </div>
                                </div>

                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="c_coupon_desc">Coupan Description</label>
                                        <textarea id="c_coupon_desc" class="form-control required" name="c_coupon_desc" rows="5" cols="33"> <?php echo $coupan_des; ?></textarea>
                                        
                                    </div>
                                </div>
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="status" value="1" id="exampleCheck1" <?php if($coupan_status == 1){ ?> checked <?php } ?>>
                                        <label class="form-check-label" for="active" >Active</label>
                                    </div>
                                </div>
                                </div>
                            </div>
                         
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
$(document).ready(function() {
CKEDITOR.replace( 'c_coupon_desc');
});
</script>