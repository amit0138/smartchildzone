<?php

$subjectId = '';
$name = '';

if(!empty($videoInfo))
{
    foreach ($videoInfo as $video)
    {
        $video_id = $video->id;
        $name = $video->name;
        $topic_id = $video->topic_id;
        $subject_id = $video->subject_id;
        $embeded_code = $video->embeded_code;
        $video_url = $video->video_url;
    }


}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Classes Management
        <small>Add / Edit Class</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-9">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Video  Edit</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="editVideo" action="<?php echo base_url() ?>video/updateVideo" method="post" role="form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Title</label>
                                        <input type="text" class="form-control required" placeholder="Class title" id="name" value="<?= $name?>" name="name" maxlength="128">
                                    </div>
                                    
                                </div>
                                <input type="hidden" name="video_id" value="<?= $video_id?>">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="subject">Subject</label>
                                        <select class="form-control required" name="subject" id="subject" required>
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subjects as  $value) { 
                                                $selectedSubject = $value->id==$subject_id?'selected':'';
                                            ?>
                                            <option value="<?=$value->id?>" <?= $selectedSubject ?> ><?php echo $value->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="class">Classes</label>
                                        <select id="class" name="class" class="form-control required">
                                            <option>Select Classes</option>
                                            <?php foreach ($classes as  $value) { 
                                                $selectedSubject = $value->id==$subject_id?'selected':'';
                                            ?>
                                            <option value="<?=$value->id?>" <?= $selectedSubject ?> ><?php echo $value->name;?></option> 
                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="topic">Topic</label>
                                        <select id="topic" name="topic" class="form-control required">
                                            <option>Select Topic</option>
                                            <?php foreach ($topics as  $value) { 
                                                $selectedTopic = $value->id==$topic_id?'selected':'';
                                            ?>
                                            <option value="<?=$value->id?>" <?= $selectedTopic ?> ><?php echo $value->name;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <div class="col-md-6">                                
                                <div class="form-group">    
                                    <video width="320" height="240" controls src="<?php echo base_url(); ?><?= $video_url ?>">
                                    </video>
                                </div>
                            </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="video">Upload Video</label>
                                        <input type="file" id="video" name="video" accept="video/*">
                                       <input type="hidden" name="edit_video" value="<?= $video_url ?>">
                                            
                                    </div>
                                    
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">                                
                                    <div class="form-group">
                                        <label for="yt">Youtube/Vimeo Embeded Code</label>
                                        
                                            
                                    </div>
                                    
                                </div>
                                
                                <div class="col-md-9">                                
                                    <div class="form-group">
                                        <textarea name="youtube_video" cols="50" rows="5">
                                            <?= $embeded_code?>
                                        </textarea>
                                            
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).on('change','#month',function(e){
        var option_val = $('#month').val();
           
            var html_option = '<option value="">Select</option>';

            if(option_val !=''){

                requestcallbackurl = baseURL+'getMonthBySubject';
                $.ajax({
                    type : "POST",
                    dataType : "json",
                    url : requestcallbackurl,
                    data : { id : option_val } 
                }).then(function(response) {
                    console.log(response);
                   
                    $.each(response, function(index, item) {
                        if(item.id !=''){
                            html_option = html_option+'<option value="'+item.id+'">'+item.name+'</option>'; 
                        }
                    });

                    $('#subject').html(html_option);

                });
            }
    });
</script>