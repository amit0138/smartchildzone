<?php include('include/header.php'); ?>
<!---------banner----------->

<div class="stz-lightblue-bg">
    <div class="stz-banner-wrapp">
        <div class="container">
            <div class="row">
                <div class="stz-section-title-banner">
                    <h3>Subject</h3>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="main-container sec-pad3 math-sec subject-sec lightblue-bg relative">
    <div class="std-bg-shape left-shape"></div>
    <div class="std-bg-shape right-shape"></div>
    <div class="container">
        <div class="row mt20">
            <div class="col-sm-12">
                <div class="stz-section-title-info">

                    <h5>Lorem Ipsum is simply dummy</h5>
                    <p> 

                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    </p> 

                </div>
            </div>
        </div>
        <div class="row mt20 stz-row">
            <div class="col-sm-12">
                <?php
                    $colorArr = COLOR_ARRAY;
                    $i = 0; 
                    foreach($subjects as $key => $subject){
                        if($colorArr[$i] >$key){
                            $i = 0;
                        }
                ?>
                    <div class="class-row table-layout shadow">
                        <div class="class-label <?= $colorArr[$i]?>  table-col vmid">
                            <div class="class-tag"><?= $key+1; ?></div>
                        </div>
                        <div class="class-content col-sm-12 table-col vmid darkestgrayclr">
                            <h3 class="heading3"><?= $subject->name ?></h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type</p>

                        </div>
                        <div class="btn-block table-col vmid robotolight"> <a href="<?php echo base_url() ?>videos/<?= $slug ?>/<?= $subject->slug ?>" class="class-btn <?= $colorArr[$i]?>">Read More</a> </div>
                    </div>
                <?php
                    $i++; 
                    }
                ?>        
            </div>
        </div>
    </div>
</section>
<?php include('include/footer.php'); ?>