<?php include('include/header.php'); ?>
<!---------banner----------->
<div class="stz-lightblue-bg">
    <div class="stz-banner-wrapp time-table-bg">
        <div class="container">
            <div class="row">
                <div class="stz-section-title-banner">
                    <h3>Videos</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="month-selection">
    <div class="col-sm-12">
        <div class="stz-section-title-info">
            <h5>Lorem Ipsum is simply dummy</h5>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
            </p>
        </div>
    </div>

    <div class="std-bg-shape left-shape"></div>
    <div class="std-bg-shape right-shape"></div>

    <div class="container">
        <div class="row videos-wras-row">
            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/6GMAugzV5ls" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="wrapp-vidos-info">
                        <h3 class="std-yellow">Why do we use it</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/kCHeKc2R96o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>

                <div class="wrapp-vidos-info">
                    <h3 class="std-orange">Why do we use it</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/TkAku5DS4fY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>

                <div class="wrapp-vidos-info">
                    <h3 class="std-red">Why do we use it</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/dTQVzcwhsSc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>

                <div class="wrapp-vidos-info">
                    <h3 class="std-green">Why do we use it</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/dTQVzcwhsSc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>

                <div class="wrapp-vidos-info">
                    <h3 class="std-green">Why do we use it</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/6GMAugzV5ls" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <div class="wrapp-vidos-info">
                        <h3 class="std-yellow">Why do we use it</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/TkAku5DS4fY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>

                <div class="wrapp-vidos-info">
                    <h3 class="std-red">Why do we use it</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>

            <div class="col-md-3 col-sm-3">
                <div class="wrapp-vidos">
                    <iframe src="https://www.youtube.com/embed/kCHeKc2R96o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>

                <div class="wrapp-vidos-info">
                    <h3 class="std-orange">Why do we use it</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('include/footer.php'); ?>