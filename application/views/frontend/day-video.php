  <?php include('include/header.php'); ?>
  <!-- bottom navigation -->

  <!---------banner----------->
  <div class="stz-lightblue-bg">
      <div class="stz-banner-wrapp time-table-bg">
          <div class="container">
              <div class="row">
                  <div class="stz-section-title-banner">
                      <h3>Time Table</h3>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <section class="month-selection">
      <div class="col-sm-12">
          <div class="stz-section-title-info">

              <h5>Lorem Ipsum is simply dummy</h5>
              <p> 

                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
              </p> 

          </div>
      </div>



      <div class="std-bg-shape left-shape"></div>
      <div class="std-bg-shape right-shape"></div>


      <div class="container">
          <div class="row">
              <div class="selection-coloumn">
                <?php 
                    $colorArr = COLOR_ARRAY;
                    $i = 0;
                    foreach($days as $key=> $day) {
                        if($colorArr[$i] >$key){
                            $i = 0;
                        }
                ?>
                      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 time-custom-col col-2">
                        
                          <div class="custom-column text-center">
                              <a href="<?php echo base_url() ?>videos/<?= $slug?>/<?= $slug1?>/<?= $day->slug ?>" class="time-box <?= $colorArr[$i]?>">
                                  <h4>DAY <?= $day->name ?></h4>
                              </a>
                          </div>
                      </div>
                <?php 
                  $i++;
                    }     
                ?>

              </div>
          </div>
      </div>
  </section>
  <?php include('include/footer.php'); ?>