<?php include('include/header.php'); ?>
<!---------banner----------->
<div class="stz-lightblue-bg">
  <div class="stz-banner-wrapp">
    <div class="container">
      <div class="row">
        <div class="stz-section-title-banner">
          <h3>Learning Plan</h3>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="main-container sec-pad3 math-sec subject-sec lightblue-bg relative">
  <div class="std-bg-shape left-shape"></div>
  <div class="std-bg-shape right-shape"></div>
  <div class="container">
    <div class="row mt20">
      <div class="col-sm-12">
        <div class="stz-section-title-info">
          <h5>Select Month</h5>
          <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
        </div>
      </div>
    </div>
    <div class="row mt20 stz-row">
      <div class="col-sm-12">
        <?php 
        $colorArr = COLOR_ARRAY;
        $i = 0;
        foreach($months as $key=> $month){  
          if($colorArr[$i] >$key){
            $i = 0;
          }
        ?>
        <div class="col-md-3 col-sm-3">
            <div class="stz-wrapp-boxsmonth <?= $colorArr[$i]?>"> <a href="<?php echo base_url() ?>learning-plan/<?= $month->slug?>">
              <div class="stz-wrapp-icon"> <img src="<?php echo base_url() ?>assets/images/stz-icon01.png" alt=""> </div>
                <h4><?= $month->name ?></h4>
                </a> 
            </div>
          </div>
        <?php 
        $i++;
      }
       ?>  

        
        
      </div>
    </div>
  </div>
</section>
<section class="main-container join-now-sec lightblue-bg relative">
  <div class="botshape-img"><img src="https://www.smartchildzone.com/uploads/page/2042723682.png"></div>
</section>


<?php include('include/footer.php'); ?>