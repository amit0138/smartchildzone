<style type="text/css">
        .box-min-tab{width: 100%; position: relative; overflow: hidden; margin-top: 30px;}
      .img-box-tab {
  height: 230px;
  overflow: hidden;
  border-radius: 5px;
}
        .img-box-tab img{o-object-fit: cover;
object-fit: cover;
-ms-flex-negative: 0;
flex-shrink: 0;
-webkit-box-flex: 0;
-ms-flex-positive: 0;
flex-grow: 0;
height: 100%}
    
        .box-min-tab a {
  position: absolute;
  top: 0;
  /* bottom: 0; */
  margin: auto;
  background: rgba(0,0,0,0.6);
  width: 100%;
  /* height: 50px; */
  text-align: center;
  color: #FFF;
  font-size: 13px;
  padding: 9px 10px;
  margin-top: 46%;
  overflow: hidden;
  text-transform: uppercase;
}

@media (min-width:992px){
    .col-lg-20{width: 20%;}
  }

      </style>
<div id="subject_div" class="container" style="padding-top: 30px; display:none">
  <div class="row">
    <h3> Select Subject</h3>
    <div class="col-md-4 col-lg-20">
      <div class="box-min-tab">
        <div class="img-box-tab"> <img src="http://demo.host4india.in/czone/assets/img/tab1.jpg" width="100%"> </div>
        <a href="javascript:void(0);" onclick="$('#class_div').show();$('#subject_div').hide();$('#subject').val('4');">Subect 1</a> </div>
    </div>
  </div>
</div>
<div id="class_div" class="container" style="padding-top: 30px; display:none">
  <div class="row">
    <h3> Select Class</h3>
    <div class="col-md-4 col-lg-20">
      <div class="box-min-tab">
        <div class="img-box-tab"> <img src="http://demo.host4india.in/czone/assets/img/tab1.jpg" width="100%"> </div>
        <a href="javascript:void(0);" onclick="$('#class_div').hide();$('#day_div').show();$('#class').val('5');">Class 1</a> </div>
    </div>
  </div>
</div>
<div id="day_div" class="container" style="padding-top: 30px; display:none">
  <h3> Select Day</h3>
  <div class="row">
    <div class="col-md-4 col-lg-20">
      <div class="box-min-tab">
        <div class="img-box-tab"> <img src="http://demo.host4india.in/czone/assets/img/tab1.jpg" width="100%"> </div>
        <a href="javascript:void(0);" onclick="$('#day').val('5');document.time_table.submit();">Day 1</a> </div>
    </div>
  </div>
</div>
<form id="time_table" name="time_table" method="post">
  <input type="hidden" name="day" id="day" value="" />
  <input type="hidden" name="subject" id="subject" value="" />
  <input type="hidden" name="month" id="month" value="" />
  <input type="hidden" name="class" id="class" value="" />
</form>
<footer class="footer">
  <div class="container">
    <div class="row footercontent sec-pad">
      <div class="col-md-3">
        <ul class="footer-listing">
          <li><a class="font14 blue" href="https://www.smartchildzone.com/membership">Membership</a></li>
          <li><a class="font14 blue" href="https://www.smartchildzone.com/award">Award</a></li>
          <li><a class="font14 blue" href="https://www.smartchildzone.com/recommendation">Recommendation</a></li>
          <li><a class="font14 blue" href="https://www.smartchildzone.com/diagnostic">Diagnostic</a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul class="footer-listing">
          <li><a class="font14 blue" href="https://www.smartchildzone.com/about-us">About Us</a></li>
          <li><a class="font14 blue" href="https://www.smartchildzone.com/career">Career</a></li>
          <li><a class="font14 blue" href="https://www.smartchildzone.com/blog/">Blog</a></li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul class="footer-listing">
          <li><a class="font14 blue font6" href="https://www.smartchildzone.com/contact">Contact Us</a></li>
          <li><a class="font14 blue font6" href="https://www.smartchildzone.com/faq">FAQ</a></li>
          <li><a class="font14 blue" href="https://www.smartchildzone.com/support">Support</a></li>
        </ul>
      </div>
      <div class="col-md-3 social">
        <div class="socialpopup">
          <ul>
            <li class="font14 followus white text-uppercase">Follow Us<i class="fa fa-angle-double-down ml10"></i></li>
            <li><a target="_blank" href="https://web.facebook.com/Smart-Child-Zone-105836404360772/?modal=admin_todo_tour"><i class="fa fa-facebook"></i>Facebook</a></li>
            <li><a target="_blank" href="https://twitter.com/SmartChildZone1"><i class="fa fa-twitter"></i>Twitter</a></li>
            <li><a target="_blank" href="https://www.instagram.com/smartchildzone/"><i class="fa fa-instagram "></i>Instagram</a></li>
            <li><a target="_blank" href="https://www.pinterest.co.uk/0em77gt0kywr5ld1idcrelnfwv8eyw/"><i class="fa fa-pinterest-p"></i>Pinterest</a></li>
            <li><a target="_blank" href="https://www.youtube.com/channel/UCvKhNyTxBA3kBbJHHS1Pxow?view_as=subscriber"><i class="fa fa-youtube"></i>Youtube</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-2">
        <div id="google_translate_element"></div>
      </div>
    </div>
  </div>
  <div class="footerbottom">
    <div class="container">
      <div class="row">
        <div class="footer-bot">
          <div class="col-md-4"> <img src="https://www.smartchildzone.com/uploads/settings/1388271330.png" class="inone mr10 footerlogo" />
            <p class="font14 blue inone">© 2019 SmartChildZone. All Right Reserved.</p>
          </div>
          <div class="col-md-2"> </div>
          <div class="col-md-6 footernav"> <a href="https://www.smartchildzone.com/privacy-policy" class="font14 blue mr10">Policy and Cookie Policy</a> <a href="https://www.smartchildzone.com/term-n-condition" class="font14 blue">Term & Conditions</a> </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- Back to Top Scroller -->
<div id="back-top" class="back-top" style="display: none;"><a href="#"><i class="fa fa-long-arrow-up"></i></a></div>

<!-- Modal -->
<div class="modal fade formmodal" id="subject_check" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upgrade Now</h4>
      </div>
      <div class="modal-body">
        <h6 class="font18 mb20 text-center"> You are previewing . <a href="https://www.smartchildzone.com/membership-plan" class="std-lightgreen">Upgrade now </a> for unlimited practice. </h6>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade formmodal" id="subjectform" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select subject and grade </h4>
      </div>
      <div class="modal-body">
        <form name="submit_fo" method="post" action="https://www.smartchildzone.com/recommendation">
          <div class="formcontainer">
            <div class="form-group rel">
              <select required name="subject" id="subject_c" class="form-control" >
                <option value="">Select</option>
                <option value="1">Math</option>
                <option value="3">Science</option>
              </select>
            </div>
            <div class="form-group rel">
              <select required name="class" id="class_c" class="form-control" >
                <option value="">Select</option>
              </select>
            </div>
            <button type="submit" class="gradientbtn loginbtn noradius">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Scritps --> 
<!-- Modal --> 
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
<!-- <script src="https://www.smartchildzone.com/assets/frontend/js/jquery.min.js"></script> --> 
<script src="https://www.smartchildzone.com/assets/frontend/js/bootstrap.min.js"></script> 
<script src='https://isotope.metafizzy.co/isotope.pkgd.js'></script> 
<script src="https://www.smartchildzone.com/assets/frontend/js/owl.carousel.min.js"></script> 
<script src="https://www.smartchildzone.com/assets/frontend/js/gallery.js"></script> 
<script src="https://www.smartchildzone.com/assets/frontend/js/custom.js"></script> 
<script src="https://www.smartchildzone.com/assets/frontend/js/wow.min.js"></script> 
<script src="https://www.smartchildzone.com/assets/frontend/js/main.js"></script> 
<script src="https://www.smartchildzone.com/assets/frontend/js/select2.min.js"></script> 
<script>
        $(document).ready(function () {
            $('.select_box').select2();
        });
    </script> 
<script>
           $(document).ready(function () {
                	$('#subject_c').change(function(){
            var elm21 =  $(this).val();
            var elm212 =  $('#class_c');
             $.post( 'Ajax/class_list/'+elm21, function( data ) {
                    elm212.html(data);
                    });
          
           });
           });
          
                 $('.coupon-submit').click(function(){
      //alert("asdfs");
        var courseid_coupon    = $("#set_pack_price_other").text();
        var courseid_couponother    = $("#set_pack_price").text();
        var coupon_input  = $(".coupon_input").val();
        
        $.ajax({
            url : "https://www.smartchildzone.com/home/couponcode",
            method : "POST",
            dataType: "json",
            data : {courseid_coupon: courseid_coupon, coupon_input: coupon_input, courseid_couponother: courseid_couponother},
            success: function(data){
               $('.showcodemessage').html(data.err);
               $("#set_pack_price_other").html(data.pp);
               $("#set_pack_price").html(data.pp_other);
               $("#coupon_codeprice").val(data.cc);
//                if(data.suc=='1'){
//                location.reload();
//                }
            }
        });
});
    
          
         wow = new WOW({
         animateClass: 'animated',
         offset: 100,
         callback: function(box) {
         console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
         }
         });
         wow.init();
      </script> 
<script type="text/javascript">
   
function googleTranslateElementInit() {
  new google.translate.TranslateElement('google_translate_element');
}
</script> 
<script>
         // Get the modal
         var modal = document.getElementById('loginform');
         // When the user clicks anywhere outside of the modal, close it
         window.onclick = function(event) {
         if (event.target == modal) {
         modal.style.display = "none";
         }
         }
         var modal = document.getElementById('signupform');
         // When the user clicks anywhere outside of the modal, close it
         window.onclick = function(event) {
         if (event.target == modal) {
         modal.style.display = "none";
         }
         }
         $('#loginform').on('show.bs.modal', function(e) {
         $('body').addClass('stylemodal');
         });
              setInterval(function(){ 
      $(".goog-te-combo option[value='']").remove();
    }, 50);
      </script> 
<script>
         $(document).ready(function() {
         // Mobile Navigation
         $(".navbar-toggle").click(function() {
         if ($(this).hasClass('checked')) {
         $(this).removeClass('checked');
         $(".navbar-collapse").removeClass('open');
         } else {
         $(this).addClass('checked');
         $(".navbar-collapse").addClass('open');
         }
         });
         $(".close-nav").click(function() {
         $('.navbar-toggle').removeClass('checked');
         $(".navbar-collapse").removeClass('open');
         });
         });
      </script> 
<script>
         $(document).ready(function() {
            var hh = '';
            if(hh){
               $('#subject_check').modal();
            }
         });
      </script>
</body>
</html>