<!DOCTYPE html>
<html lang="en">
<head>
<title>Adaptive daily Learning plan For Students ng | Educational Games Online</title>
<meta name="description" content="Smartchildzone.com is the best online educational platform for students to learn math and science. Immersive, Adaptive Learning for Students in NG. Browse more.">
<link rel="canonical" href="https://www.smartchildzone.com/" />
<!-- Meta Tags -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="google-site-verification" content="xTLT2tfK7XEHdY-YFshLX7aWTa89GRghtDTPkJ1hcNU" />

<!-- deepak css -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/stz-style-new.css" type="text/css">

<!-- Bootstrap Files -->
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/select2.min.css" type="text/css">
<link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/gallery.css" type="text/css">
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/custom-inner.css" type="text/css">
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/custom-responsive.css" type="text/css">
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/graph.css" type="text/css">
<link rel="icon" type="image/png" href="https://www.smartchildzone.com/assets/frontend/images/favicon.png">
<!-- Icon Fonts -->
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/font-awesome.css" type="text/css">
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/themify-icons.css" type="text/css">
<link rel="stylesheet" href="https://www.smartchildzone.com/assets/frontend/css/line-awesome.min.css" type="text/css">
<!-- Google Font Family -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
<!-- Owl Carousel Slider -->
<link href="https://www.smartchildzone.com/assets/frontend/css/owl.carousel.css" rel="stylesheet">
<script src="https://www.smartchildzone.com/assets/frontend/js/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

<!-- Global site tag (gtag.js) - Google Analytics -->

</head>
<body>
<!-- Header -->
<header class="header navfixedhide">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 table-layout"> 
        <!-- Logo Tab -->
        <div class="logo-tab table-col vmid">
          <div class="logo-sec"> <a href="<?php echo base_url(); ?>" class="relative inlineb"><img src="https://www.smartchildzone.com/uploads/settings/1585130188.png" alt="" class="img-responsive"> 
            <!--<span class="logo-title-txt">Smart Child Zone</span>--> 
            </a> </div>
        </div>
        <!-- ./Logo Tab --> 
        <!-- Navigation Tab -->
        <div class="navigation-tab table-col vmid headerleft">
          <div class="navigation-sec hide"> 
            <!-- Navbar -->
            <div class="inline navbar navbar-default">
              <div class="navbar-header">
                <button class="navbar-toggle" data-target="#navigation" data-toggle="collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <div class="collapse navbar-collapse " id="navigation">
                <div class="close-nav"><a href="javascript:void(0)" class="closer">×</a></div>
                <ul class="nav navbar-nav">
                  <li class="dropdown"> <a href="javscript:void(0)" class="collapsed" data-toggle="collapse" data-target="#sectordropdown">Categories<i class="dropicon fa fa-angle-down ml5"></i> <span class=""></span></a>
                    <ul class="dropdown-menu collapse" id="sectordropdown">
                      <li class="dropdown"> <a href="javscript:void(0)" class="collapsed" data-toggle="collapse" data-target="#sectordropdown3">Development</a>
                        <ul class="dropdown-menu" id="sectordropdown3">
                          <li><a href="#">Web Developemnt</a></li>
                          <li><a href="#">Game Developemnt</a></li>
                          <li><a href="#">Mobile App</a></li>
                          <li><a href="#">Ecommerce</a></li>
                        </ul>
                      </li>
                      <li class="dropdown"> <a href="#" class="collapsed" data-toggle="collapse" data-target="#sectordropdown2">Business</a>
                        <ul class="dropdown-menu" id="sectordropdown2">
                          <li><a href="#">Finance</a></li>
                          <li><a href="#">Communication</a></li>
                          <li><a href="#">Sales</a></li>
                          <li><a href="#">Management</a></li>
                        </ul>
                      </li>
                      <li><a href="#">IT & Software</a></li>
                      <li><a href="#">It Certification</a></li>
                      <li><a href="#">Netwrok & Security</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"> <a href="javscript:void(0)" class="collapsed" data-toggle="collapse" data-target="#sectordropdown1">Teach on Smart Child Zone</a>
                    <div class="dropdown-menu dropdown-box collapse text-center" id="sectordropdown1">
                      <p>Teach what you love. Smart Child Zone gives you the tool to create an online courses.</p>
                      <a href="#" class="sm-gradientbtn">Learn More</a> </div>
                  </li>
                </ul>
              </div>
            </div>
            <!-- ./Navbar --> 
          </div>
        </div>
        <div class="table-col vmid headermid">
          <form class="searchform" method="get" action="https://www.smartchildzone.com/course/search" name="search_f">
            <input type="text" class="topsearchtop" name="search_keyword" placeholder="What do you want to learn?">
            <button type="submit"><i class="serchbtn fa fa-search"></i></button>
          </form>
        </div>
        <div class="table-col vmid headerright ">
          <ul class="nav navbar-nav navbar-right">
            <li class="inlineb"><a href="#" data-toggle="modal" data-target="#loginform" class="linkbtn  loginbtn"><i class="navicon ti-user mr8"></i><span>Log In</span></a></li>
            <!-- <li class="inlineb"><a href="#" data-toggle="modal" data-target="#signupform" class="linkbtn signupbtn"><i class="navicon ti-pencil-alt mr8"></i><span>Sign Up</span></a></li> -->
            <li class="inlineb"><a href="https://www.smartchildzone.com/families/become-a-member" class="linkbtn signupbtn"><i class="navicon ti-pencil-alt mr8"></i><span>Sign Up</span></a></li>
          </ul>
        </div>
        <!-- ./Navigation Tab --> 
      </div>
    </div>
  </div>
</header>

<div id="loginform" class="modal fade formmodal login-credentials" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" title="close">&times;</button>
        <h5 class="modal-title font18 fweight5">Login to your Smart Child Zone  Account!</h5>
      </div>
      <div class="modal-body">
        <label class="errMsg" id="limit_err"></label>
        <form name="signin" action="https://www.smartchildzone.com/Login/index" method="POST">
          <input type="hidden" name="_token" value="uOSWNR16x841Jb36o6PAoxedTl1giPX55Sbph4my">
          <div class="formcontainer">
            <div class="form-group rel">
              <input type="text" placeholder="User Name" name="email" id="email">
              <span class="formicon fa fa-user fa-lg"></span> </div>
            <div class="form-group rel">
              <input type="password" placeholder="Password" name="password" id="password">
              <span class="formicon fa fa-lock fa-lg"></span> </div>
            <button type="submit" class="gradientbtn loginbtn noradius"><i class="glyphicon glyphicon-refresh glyphicon-refresh-animate mr10 font14"></i>Log In</button>
            <div class="text-center forgotpswdlink mt10">Or <a href="#" data-toggle="modal" data-target="#forgotpswdform" data-dismiss="modal">Forgot Password ?</a></div>
            <div class="text-center footertxtlink">
              <p class="font12">By signing up, you agree to our <a href="https://www.smartchildzone.com/term-n-condition" class="navyclr">Term of Us</a> and <a href="https://www.smartchildzone.com/privacy-policy" class="navyclr">Privacy Policy</a>.</p>
            </div>
            <div class="text-center signuplink">Don’t have an account ?<a href="https://www.smartchildzone.com/families/become-a-member" > Sign up</a></div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="signupform" class="modal fade formmodal login-credentials" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" title="close">&times;</button>
        <h5 class="modal-title font18 fweight5">Sign Up and Start Learning</h5>
      </div>
      <div class="modal-body">
        <label class="errMsg"></label>
        <form action="https://www.smartchildzone.com/Register/signup" method="post" name="usersignup">
          <input type="hidden" name="_token" value="uOSWNR16x841Jb36o6PAoxedTl1giPX55Sbph4my">
          <div class="formcontainer">
            <div class="form-group rel">
              <input type="hidden" value="2" name="user_role" id="user_role">
              <input type="text" placeholder="Full name" name="first_name" id="first_name" >
              <span class="formicon fa fa-user fa-lg"></span> </div>
            <div class="form-group rel">
              <input type="email" placeholder="Email" name="email" id="email" >
              <span class="formicon fa fa-envelope fa-lg"></span> </div>
            <div class="form-group rel">
              <input type="password" placeholder="Password" name="password" id="password" >
              <span class="formicon fa fa-lock fa-lg"></span> </div>
            <label class="checkboxcontainer agree-terms-lable">Check here for existing deals and personalized course and recommendations
              <input type="checkbox" name="agree_terms" id="agree_terms" value="1">
              <span class="checkmark"></span> </label>
            <button type="submit" class="gradientbtn loginbtn noradius"><i class="glyphicon glyphicon-refresh glyphicon-refresh-animate mr10 font14"></i>Sign Up</button>
            <div class="text-center footertxtlink">
              <p class="font12">By signing up, you agree to our <a href="https://www.smartchildzone.com/term-n-condition" class="navyclr">Term of Us</a> and <a href="https://www.smartchildzone.com/privacy-policy" class="navyclr">Privacy Policy</a>.</p>
            </div>
            <div class="text-center signuplink">Already have an account ?<a href="#" data-toggle="modal" data-target="#loginform" data-dismiss="modal" class="navyclr"> Log In</a></div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="forgotpswdform" class="modal fade formmodal login-credentials" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" title="close">&times;</button>
        <h5 class="modal-title font18 fweight5">Forgot Password</h5>
      </div>
      <div class="modal-body">
        <label class="errMsg"></label>
        <form name="forgotPassword" method="post" action="https://www.smartchildzone.com/Login/forget_password">
          <div class="formcontainer">
            <div class="form-group rel">
              <input type="text" placeholder="Email" name="email" id="email">
              <span class="formicon fa fa-envelope fa-lg"></span> </div>
            <button type="submit" class="gradientbtn loginbtn noradius">Reset Password</button>
            <!--  <div class="row"> --> 
            <!-- </div> -->
            <div class="text-center forgotpswdlink mt10">Or <a href="#" data-toggle="modal" data-target="#loginform" data-dismiss="modal" class="navyclr font16">Log In?</a></div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- ./Header -->
<input type="hidden" id="authRedirectUrl" value="https://www.smartchildzone.com/analytics">
<input type="hidden" id="authRedirectUrl1" value="https://www.smartchildzone.com/">
<script src='https://www.google.com/recaptcha/api.js'></script> 
<!-- ./Header --> 
<!-- bottom navigation -->
<div class="botnavigation-sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 botnavigation table-layout robotolight font20">
        <div class="left-botnavigation table-col vmid">
          <ul>
            <li ><a href="<?php echo base_url(); ?>">Learning</a></li>
            <li ><a href="#">Analytics</a></li>
          </ul>
        </div>
        <div class="right-botnavigation table-col vmid"> <a href="https://www.smartchildzone.com/membership">Membership</a> </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" name="auto_base_url" id="auto_base_url" value="https://www.smartchildzone.com/">

<!-- Modal -->
<div id="parent_modal" class="modal formmodal" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Parent Secret Word</h4>
      </div>
      <div class="modal-body">
        <label class="errMsg"></label>
        <form name="parentsecret" action="#" method="POST">
          <div class="formcontainer">
            <div class="form-group rel">
              <input type="password" placeholder="Secret Word" name="parent_secret_word" id="parent_secret_word">
              <span class="formicon fa fa-lock fa-lg"></span> </div>
            <button type="submit" class="gradientbtn loginbtn noradius"><i class="glyphicon glyphicon-refresh glyphicon-refresh-animate mr10 font14"></i>Submit</button>
            <div class="text-center forgotpswdlink mt10">Or <a href="#" data-toggle="modal" data-target="#parentforget" data-dismiss="modal">Forgot Password ?</a></div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="parentforget" class="modal" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Secret Word</h4>
      </div>
      <div class="modal-body">
        <p>Did you forget your secret word? To have a new secret word sent to you by e‑mail, click <strong>Request secret word.</strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="parent_frgpass">Request Secret Word</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="parentforgetsubmit" class="modal" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Secret word sent</h4>
      </div>
      <div class="modal-body">
        <p>Your new secret word has been e‑mailed to you at .</p>
        <p>Please check your junk mail folder if you do not receive a message from Smart Child Zone shortly. You may be able to prevent messages from being incorrectly marked as spam by adding info@smartchilzone.com to your e‑mail program's address book or safe list.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="childrenforget" class="modal" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Children Forget Secret Word</h4>
      </div>
      <div class="modal-body">
        <p>Did you forget your secret word? Your parent can find it for you.</p>
        <p>Instructions for parent: Sign in to your parent account. Then select Parent Profile & settings from the welcome drop-down menu. In the profile window, scroll down to find the secret word.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="myModalafterloginpopup" class="modal" role="dialog">
  <div class="modal-dialog"> 
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header"> 
        <!--        <button type="button" class="close" data-dismiss="modal">&times;</button>-->
        <h4 class="modal-title">Welcome  !! </h4>
      </div>
      <div class="modal-body">
        <h5>Who you are ?</h5>
        <ul>
          <li><a href="https://www.smartchildzone.com/check-change-role/1">
            <div class="img-box user-placeholder"><img src="https://www.smartchildzone.com/assets/frontend/images/child-img.png" /></div>
            Parent ()</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!----------------------------Again login popup ------------------------------------>
<div class="modal fade formmodal" id="myModalagainlogin" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are You sure for Login Here ?</h4>
      </div>
      <div class="modal-body">
        <h6 class="font16 mb20 text-center"> You are already login on another system. You want to logout from another system and Login Here.<br>
          <span> Are You sure for Login Here ? </span> </h6>
      </div>
      <div class="modal-footer nobdr">
        <div class="btns-block mt0 mb20 text-center"> <a href="https://www.smartchildzone.com/Login/again-login" class="blueclr gradientbtn sm-btn">Yes</a> <a href="https://www.smartchildzone.com/" class="blueclr gradientbtn sm-btn">No</a> </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(e) {
   var base_url = $('#auto_base_url').val();
   var cache = {};
   $('.topsearchtop').autocomplete({
      source: function(request, response) {
         var term = request.term;
         if ( term in cache ) 
         {
            response( cache[ term ] );
            return;
         }

         $.ajax({
            url: base_url + "Ajax/Autocomplete",
            data: {query: term},
            success: function(data) {
            data = $.parseJSON(data);
            var transformed = $.map(data, function(el) {
               return {
                  label: el.name,
                  url: el.url
                  };
               });
               cache[ term ] = transformed;
               response(transformed);
            },
            error: function() {
               response([]);
            }
         });
      },
      select: function( event, ui ) {
         window.location=ui.item.url;
      }
   });

   $('.topsearchtop').click(function(){
      $(this).trigger('keydown');
   });


   $('#parent_frgpass').click(function(){
      var par_userid = $('#par_userid').val();
      $.ajax({
         url: base_url + "parent-request-word",
         method:'post',
         data: {'par_userid':par_userid},
         success: function(data){
            $('#parentforgetsubmit').modal();
         },
         error: function(){
         }
      });
   });

});
</script> <!-- Banner -->
<style>
  .month-selection {
  padding: 70px 0;
}
  .mb-30{margin-bottom: 30px}
  .bg-color {
  height: 225px;
  width: 100%;
}
.custom-column {
  width: 100%;
  display: inline-table;
  max-width: 100%;
}
.bg-color {
  height: 225px;
  width: 100%;
  vertical-align: middle;
  display: table-cell;
  padding: 0 15px;
}
.darg-blue-bg {
  background: #203864;
}
.yellow-bg {
  background: #fdca3b;
}
.pink-bg {
  background: #ce0c63;
}
.light-blue-bg {
  background: #296fc4;
}
.bg-color.brown-bg {
  background: #9c5b25;
}
.bg-color h4 {
  color: #fff;
  font-size: 21px;
}
  @media (min-width: 991px) {
.col-20 {
  width: 20%;
}
  }

</style>