<?php

$commissionid = '';
$coupanid = '';
$commission = '';

if(!empty($commissioninfo))
{
    foreach ($commissioninfo as $sub)
    {
        $commissionid = $sub->id;
        $coupanid = $sub->coupan_id;
        $commission = $sub->commission;
    }
}


?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Commission Management
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Commission Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>commission/updateCommission" method="post" id="editsubject" role="form">
                        <div class="box-body">
                            <div class="row">
                                <input type="hidden" value="<?php echo $commissionid; ?>" name="commissionid" id="commissionid" />    
                             

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Coupan Code</label>
                                        <select name="coupan" id="coupan" class="form-control required">
                                            <option>Select Coupan</option>
                                            <?php foreach ($coupans as  $value) { 
                                                $selectedMonth = $value->c_id==$coupanid?'selected':'';
                                                ?>
                                            <option value="<?=$value->c_id?>" <?= $selectedMonth ?> ><?php echo $value->c_coupon_code;?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="c_coupon_per">Commission</label>
                                        <input type="number" class="form-control required" placeholder="Commission" id="name" value="<?php echo $commission; ?>" name="commission" maxlength="4">
                                    </div>
                                </div>

                                </div>
                            </div>
                         
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>
