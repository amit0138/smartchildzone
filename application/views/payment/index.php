<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Payment Listing Search For Payments
        </h1>
    </section>
    <section class="content">
        <form action="<?php echo base_url() ?>payment/paymentListing" method="POST" id="searchList">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="row">


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email:</label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="text" value="" class="form-control pull-right" id="email" name="email">
                                            <input type="hidden" name="searchemail" value="<?= $searchText?>">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>

                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Search">
                            <input type="reset" class="btn btn-default" id="reset_button" value="Reset">
                        </div>
                        <div class="box-header">
                            <h3 class="box-title">Payment List</h3>

                        </div>

                        <div class="box-body table-responsive">
                            <?php 

                                if(isset($paymentRecords) && !empty($paymentRecords)){
                            ?>

                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <th>User Id</th>
                                        <th>Membership Id</th>
                                        <th>Payment Gross</th>
                                        <th>Currency Code</th>
                                        <th>Payer Email</th>
                                        <th>Payment Status</th>
                                        <th>Payment Time</th>
                                        <th>Payment Type</th>
                                        <th>Payment For</th>
                                    </tr>
                                <?php foreach ($paymentRecords as $key => $payment) { ?>    
                                    <tr>
                                        <td><?=$payment->user_id ?></td>
                                        <td><?=$payment->membership_plan?$payment->membership_plan:'N/A' ?> </td>
                                        <td><?=$payment->payment_gross?$payment->payment_gross:'N/A' ?></td>
                                        <td><?=$payment->currency_code?$payment->currency_code:'N/A' ?></td>
                                        <td><?=$payment->payer_email ?></td>
                                        <td><?=$payment->payment_status ?></td>
                                        <td><?=$payment->payment_time ?></td>
                                        <td><?=$payment->payment_type ?></td>
                                        <td><?=$payment->payment_for ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                            <?php 

                                }else{
                                    echo '<p>No Record Found.</p>';
                                }

                            ?>
                            

                        </div>  
                        <div class="box-footer clearfix">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <div class="box-footer clearfix">
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>
        </form>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "payment/paymentListing/" + value);
            jQuery("#searchList").submit();
        });
    });

    $(document).on('click','#reset_button',function(e){
        $('#email').val('');
        var requestcallbackurl = baseURL+'payment/paymentListing';
    $(location).attr('href', requestcallbackurl); // Using this
    });
</script>
