<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Subject extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('subject_model');
        
        $this->load->model('Month_model');
        
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Subject list
     */
    public function subjectListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('Subject_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Subject_model->subjectListingCount($searchText);
			$returns = $this->paginationCompress( "subject/subjectListing/", $count, 2 );
            $data['subjectRecords'] = $this->Subject_model->subjectListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Subject Listing';
            $this->loadViews("subject/index", $this->global, $data, NULL);
        }
    }

    public function addNewSubject(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('Month_model');
            $data['month'] = $this->Month_model->getAllMonth();
            $this->global['pageTitle'] = 'SmartChildZone : Add New Subject';
            $this->loadViews("subject/create", $this->global,$data, NULL);
        }
    }


    
    /**
     * This function is used to add new Subjects to the system
     */
    function storeSubject()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Subject Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewSubject();
            }
            else
            {
                
                $name  = $this->input->post('name');
                $month  = $this->input->post('month');

                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_subject');
                
                $subjectInfo = ['name'=> $name,'slug'=>$slug,'month_id'=>$month,'created_at'=>date('Y-m-d H:i:s')];

                $result = $this->subject_model->addNewSubject($subjectInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Subject created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Subject creation failed');
                }
                
                redirect('addNewSubject');
            }
        }
    }


    
    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function subjectEdit($subjectid = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($subjectid == null)
            {
                redirect('subjectListing');
            }
            
            $data['subjectInfo'] = $this->subject_model->getSubjectInfo($subjectid);
            $data['month'] = $this->Month_model->getAllMonth();
            $this->global['pageTitle'] = 'SmartChildZone : Edit Subject';
            
            $this->loadViews("subject/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateSubject()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $subjectId = $this->input->post('subjectId');
            
            $this->form_validation->set_rules('name','Subject Title','trim|required|max_length[128]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->subjectEdit($subjectId);
            }
            else
            {
                $name = $this->input->post('name');
                $month  = $this->input->post('month');

                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_subject');
                
                $subjectInfo = array();
                
                $subjectInfo = array('name'=>$name, 'slug'=>$slug,'month_id'=>$month,'updated_at'=>date('Y-m-d H:i:s'));
                
                
                $result = $this->subject_model->editSubject($subjectInfo, $subjectId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Subject updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Subject updation failed');
                }
                
                redirect('subjectListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using subjectid
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSubject()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $subjectId = $this->input->post('sub_id');
           
            $subjectInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->subject_model->deleteSubject($subjectId, $subjectInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
}

?>