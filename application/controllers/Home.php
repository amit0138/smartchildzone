<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Home extends BaseController
{
    
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        
        parent::__construct();
        $this->load->model('classes_model');
        $this->load->model('days_model');
        $this->load->model('month_model');
        $this->load->model('subject_model');
    }

    /**
     * Index Page for Learning Plan this controller.
     */
    public function index()
    {
        $data['months'] = $this->month_model->monthAlldata();
       
        $this->load->view('frontend/index',$data);

        
    }
    
    /**
     * This function used to day
     */
    function day()
    {
        $data['days'] = $this->days_model->dayAlldata();
        $this->load->view('frontend/day',$data);
    }
    
     /**
     * This function used to Classes
     */
    function class()
    {
        $data['classes'] = $this->classes_model->classAlldata();
        $this->load->view('frontend/class',$data);
    }

    /**
     * This function used to Classes
     */
    function subject()
    {
        $slug = $this->uri->segment(2);
        $slug1 = $this->uri->segment(3);
        $slug2 = $this->uri->segment(4);
        $slug3 = $this->uri->segment(5);
        $slug4 = $this->uri->segment(6);
        
        if(empty($slug)){
            /*$status = 'error';
            setFlashData($status, "It seems an error while sending your details, try again.");
            return redirect("/");*/
            $data['months'] = $this->month_model->monthAlldata();
       
            $this->load->view('frontend/index',$data);
        }

        if(!empty($slug4)){
            $data['slug4'] = $slug4;
            $data['slug3'] = $slug3;
            $data['slug2'] = $slug2;
            $data['slug1'] = $slug1;
            $data['slug'] = $slug;
            $data['subtopics'] = $this->getRecordBySlug($slug4,' tbl_topic','tbl_subtopic');
            $this->load->view('frontend/subtopic',$data);
        }elseif(!empty($slug3)){
            $data['slug3'] = $slug3;
            $data['slug2'] = $slug2;
            $data['slug1'] = $slug1;
            $data['slug'] = $slug;
            $data['topics'] = $this->getRecordBySlug($slug3,' tbl_days','tbl_topic');
            $this->load->view('frontend/topic',$data);

        }elseif(!empty($slug2)){

            $data['slug2'] = $slug2;
            $data['slug1'] = $slug1;
            $data['slug'] = $slug;
            $data['days'] = $this->getRecordBySlug($slug2,' tbl_classes','tbl_days');
            $this->load->view('frontend/day',$data);
            

        }elseif(!empty($slug1)){
            $data['slug1'] = $slug1;
            $data['slug'] = $slug;
            $data['classes'] = $this->getRecordBySlug($slug1,' tbl_subject','tbl_classes');

            $this->load->view('frontend/class',$data);
            
        }elseif(!empty($slug)){
            $data['slug'] = $slug;
            $data['subjects'] = $this->getRecordBySlug($slug,' tbl_months','tbl_subject');

            $this->load->view('frontend/subject',$data);
        }
        
    }

    /**
     * This function used to Classes
     */
    function videos(){
        $slug = $this->uri->segment(2);
        $slug1 = $this->uri->segment(3);
        $slug2 = $this->uri->segment(4);
        $slug3 = $this->uri->segment(5);
        $slug4 = $this->uri->segment(6);

        if(empty($slug)){
            /*$status = 'error';
            setFlashData($status, "It seems an error while sending your details, try again.");
            return redirect("/");*/
            $data['subjects'] = $this->subject_model->subjectAlldata();
            $this->load->view('frontend/subject-video',$data);
        }
        if(!empty($slug2)){
            $data['slug2'] = $slug2;
            $data['slug1'] = $slug1;
            $data['slug'] = $slug;
            $data['topics'] = $this->getRecordBySlug($slug2,' tbl_days','tbl_topic');
            $this->load->view('frontend/topic-video',$data);
        }elseif(!empty($slug3)){
            $data['slug3'] = $slug3;
            $data['slug2'] = $slug2;
            $data['slug1'] = $slug1;
            $data['slug'] = $slug;
            $data['video'] = $this->getRecordBySlug($slug3,' tbl_topic','tbl_video');
            //$this->load->view('frontend/front-videos.php',$data);
            $this->load->view('frontend/day-video',$data);
        }elseif(!empty($slug1)){
            $data['slug1'] = $slug1;
            $data['slug'] = $slug;
            $data['classes'] = $this->getRecordBySlug($slug1,'tbl_subject','tbl_classes');
            $this->load->view('frontend/class-video',$data);
        }elseif(!empty($slug)){
            $data['slug'] = $slug;
            $data['classes'] = $this->getRecordBySlug($slug,'tbl_subject','tbl_classes');
            print_r($data['classes']);exit;
            $this->load->view('frontend/class-video',$data);
        }else{
            $data['subjects'] = $this->subject_model->subjectAlldata();
            $this->load->view('frontend/subject-video',$data);
        }
    }
    
    
    /**
     * This function used to logged in user
     */
    public function loginMe()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            $result = $this->login_model->loginMe($email, $password);
            
            if(count($result) > 0)
            {
                foreach ($result as $res)
                {
                    $sessionArray = array('userId'=>$res->userId,                    
                                            'role'=>$res->roleId,
                                            'roleText'=>$res->role,
                                            'name'=>$res->name,
                                            'isLoggedIn' => TRUE
                                    );
                                    
                    $this->session->set_userdata($sessionArray);
                    
                    redirect('/dashboard');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                redirect('/login');
            }
        }
    }

    /**
     * This function used to load forgot password view
     */
    public function forgotPassword()
    {
        $this->load->view('forgotPassword');
    }
    
    /**
     * This function used to generate reset password request link
     */
    function resetPasswordUser()
    {
        $status = '';
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('login_email','Email','trim|required|valid_email|xss_clean');
                
        if($this->form_validation->run() == FALSE)
        {
            $this->forgotPassword();
        }
        else 
        {
            $email = $this->input->post('login_email');
            
            if($this->login_model->checkEmailExist($email))
            {
                $encoded_email = urlencode($email);
                
                $this->load->helper('string');
                $data['email'] = $email;
                $data['activation_id'] = random_string('alnum',15);
                $data['createdDtm'] = date('Y-m-d H:i:s');
                $data['agent'] = getBrowserAgent();
                $data['client_ip'] = $this->input->ip_address();
                
                $save = $this->login_model->resetPasswordUser($data);                
                
                if($save)
                {
                    $data1['reset_link'] = base_url() . "resetPasswordConfirmUser/" . $data['activation_id'] . "/" . $encoded_email;
                    $userInfo = $this->login_model->getCustomerInfoByEmail($email);

                    if(!empty($userInfo)){
                        $data1["name"] = $userInfo[0]->name;
                        $data1["email"] = $userInfo[0]->email;
                        $data1["message"] = "Reset Your Password";
                    }

                    $sendStatus = resetPasswordEmail($data1);

                    if($sendStatus){
                        $status = "send";
                        setFlashData($status, "Reset password link sent successfully, please check mails.");
                    } else {
                        $status = "notsend";
                        setFlashData($status, "Email has been failed, try again.");
                    }
                }
                else
                {
                    $status = 'unable';
                    setFlashData($status, "It seems an error while sending your details, try again.");
                }
            }
            else
            {
                $status = 'invalid';
                setFlashData($status, "This email is not registered with us.");
            }
            redirect('/forgotPassword');
        }
    }

    // This function used to reset the password 
    function resetPasswordConfirmUser($activation_id, $email)
    {
        // Get email and activation code from URL values at index 3-4
        $email = urldecode($email);
        
        // Check activation id in database
        $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);
        
        $data['email'] = $email;
        $data['activation_code'] = $activation_id;
        
        if ($is_correct == 1)
        {
            $this->load->view('newPassword', $data);
        }
        else
        {
            redirect('/login');
        }
    }
    
    // This function used to create new password
    function createPasswordUser()
    {
        $status = '';
        $message = '';
        $email = $this->input->post("email");
        $activation_id = $this->input->post("activation_code");
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('password','Password','required|max_length[20]');
        $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->resetPasswordConfirmUser($activation_id, urlencode($email));
        }
        else
        {
            $password = $this->input->post('password');
            $cpassword = $this->input->post('cpassword');
            
            // Check activation id in database
            $is_correct = $this->login_model->checkActivationDetails($email, $activation_id);
            
            if($is_correct == 1)
            {                
                $this->login_model->createPasswordUser($email, $password);
                
                $status = 'success';
                $message = 'Password changed successfully';
            }
            else
            {
                $status = 'error';
                $message = 'Password changed failed';
            }
            
            setFlashData($status, $message);

            redirect("/login");
        }
    }

    public function month($slug = null)
    {
        if(!$slug){
            $status = 'error';
            setFlashData($status, "It seems an error while sending your details, try again.");
            return redirect("/");
        }

        $data = $this->getRecordBySlug($slug,' tbl_months','tbl_subject');

        print_r($data);exit;

    }


}

?>