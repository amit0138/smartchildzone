<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Classes extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Classes_model');
        $this->load->model('Subject_model');
        $this->load->model('Month_model');
        $this->isLoggedIn();   
    }

    public function index()
    {
        $this->isLoggedIn();
    }
    
    
    
    /**
     * This function is used to load the Subject list
     */
    public function classesListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('classes_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Classes_model->classesListingCount($searchText);
            $returns = $this->paginationCompress( "classesListing/", $count, 5 );
            $data['classesRecords'] = $this->Classes_model->classesListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Classes Listing';
            $this->loadViews("classes/index", $this->global, $data, NULL);
        }
    }

    public function addNewClasses(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['month'] = $this->Month_model->getAllMonth();
            $this->global['pageTitle'] = 'SmartChildZone : Add New Class';
            $this->loadViews("classes/create", $this->global, $data);
        }
    }


    
    /**
     * This function is used to add new Classes to the system
     */
    function storeClasses()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Class Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewClasses();
            }
            else
            {
                
                $name  = $this->input->post('name');
                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');

                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_classes');

                $monthInfo = ['name'=> $name,'month_id'=>$month,'slug'=>$slug, 'subject_id'=>$subject,'created_at'=>date('Y-m-d H:i:s')];
                
                $this->load->model('classes_model');
                $result = $this->Classes_model->addNewClasses($monthInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Classes created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Classes creation failed');
                }
                
                redirect('addNewClasses');
            }
        }
    }


    
    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function classesEdit($class_id = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($class_id == null)
            {
                redirect('classesListing');
            }
            
            $data['classInfo'] = $this->Classes_model->getClassesInfo($class_id);

            $data['subjects'] = $this->Subject_model->getAllSubject();
            $data['months'] = $this->Month_model->getAllMonth();
            
            $this->global['pageTitle'] = 'SmartChildZone : Edit Classes';
            
            $this->loadViews("classes/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateClasses()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $classId = $this->input->post('class_id');
            
            
            $this->form_validation->set_rules('name','Classes Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('name','Class Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->subjectEdit($subjectId);
            }
            else
            {
                $name = $this->input->post('name');
                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');
                
                $subjectInfo = array();

                $monthInfo = ['name'=> $name,'month_id'=>$month,'subject_id'=>$subject,'updated_at'=>date('Y-m-d H:i:s')];
                
                $result = $this->Classes_model->editClasses($monthInfo, $classId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Classes updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Classes updation failed');
                }
                
                redirect('classesListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using subjectid
     * @return boolean $result : TRUE / FALSE
     */
    function deleteClasses()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $subjectId = $this->input->post('sub_id');
           
            $subjectInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->subject_model->deleteSubject($subjectId, $subjectInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function getMonthBySubject()
    {
        $id = $this->input->post('id');

        $subject = $this->Subject_model->getMonthBySubject($id);

        echo json_encode($subject);
    }

    function getSubjectByClasses()
    {
        $id = $this->input->post('id');

        $subject = $this->Classes_model->getSubjectByClasses($id);

        echo json_encode($subject);
    }

    function getDaysByClasses()
    {
        $id = $this->input->post('id');

        $subject = $this->Classes_model->getDaysByClasses($id);

        echo json_encode($subject);
    }

    function getTopicByDay()
    {
        $id = $this->input->post('id');

        $subject = $this->Classes_model->getTopicByDay($id);

        echo json_encode($subject);
    }

    
    function getTopicByClass()
    {
        $id = $this->input->post('id');

        $topic = $this->Classes_model->getTopicByClass($id);
       
        echo json_encode($topic);
    }

    function getSubTopicByTopic()
    {
        $id = $this->input->post('id');

        $topic = $this->Classes_model->getSubTopicByTopic($id);
       
        echo json_encode($topic);
    }

    function updateSlugByTitle($tbl){

        $result = $this->Classes_model->getModalValue($tbl);
        
        if(!empty($result)){
            foreach ($result as $key => $value) {
               $name = 'Day '.$value->name;
               //get slug from title
                $slug  = $this->create_unique_slug($name,$tbl);
                $update = ['slug'=>$slug];
                $update = $this->Classes_model->getUpdateSlug($tbl,$value->id,$update);

            }

            echo 'slug updated';exit;
        }else{

        }

    }

    function testRoute($slug)
    {
        print_r($slug);exit;
    }

    function randomName() {
        $names = array(
            'Juan typesetting',
            'Luis standard',
            'Andrew alexa',
            'Pedro jhon',
            'Watson simply',
        // and so on

        );
        return $names[rand ( 0 , count($names) -1)];
    }
   
}

?>