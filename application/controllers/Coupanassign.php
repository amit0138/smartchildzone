<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Coupanassign extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Coupanassign_model');
        $this->load->model('Coupans_model');
        $this->load->model('Marketer_model');
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Coupans list
     */
    public function coupansassignListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Coupanassign_model->coupansassignListingCount($searchText);
			$returns = $this->paginationCompress( "coupansListing/", $count, 5 );
            $data['coupansassignRecords'] = $this->Coupanassign_model->coupansassignListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Coupans Listing';
            $this->loadViews("coupansassign/index", $this->global, $data, NULL);
        }
    }

    public function addNewCoupansassign(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['coupans'] = $this->Coupans_model->getAllCoupans();
            $data['marketers'] = $this->Marketer_model->getMarkterlist();
            $this->global['pageTitle'] = 'SmartChildZone : Add New coupans';
            $this->loadViews("coupansassign/create", $this->global,$data, NULL);
        }
    }


    
    /**
     * This function is used to add new Coupans to the system
     */
    function storeCoupansassign()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('coupon_id','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('marketer_id','Markter','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewCoupans();
            }
            else
            {
                
                $coupan_code  = $this->input->post('coupon_id');
                $marketer  = $this->input->post('marketer_id');
                
                $coupansInfo = ['coupon_id'=> $coupan_code,'mar_id'=>$marketer,'created_at'=>date('Y-m-d H:i:s')];
               
                $this->load->model('coupans_model');
                $result = $this->coupans_model->addNewCoupans($coupansInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Coupans Assign To Markter created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Coupans Assign creation failed');
                }
                
                redirect('coupansassign/coupansassignListing');
            }
        }
    }


    
    /**
     * This function is used load coupans edit information
     * @param number $coupansid : Optional : This is coupans id
     */
    function coupansassignEdit($ca_id = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($ca_id == null)
            {
                redirect('coupansListing');
            }
            
            $data['coupansInfo'] = $this->coupans_model->getCoupanassignInfo($ca_id);
            
            $this->global['pageTitle'] = 'SmartChildZone : Edit Coupans Assign';
            
            $this->loadViews("coupans/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateCoupansassign()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $coupansId = $this->input->post('coupansId');
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('coupon_id','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('marketer_id','Markter','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->coupansEdit($coupansId);
            }
            else
            {
                $coupan_code  = $this->input->post('coupon_id');
                $marketer  = $this->input->post('marketer_id');
                $coupansInfo = array();
                $coupansInfo = ['coupon_id'=> $coupan_code,'mar_id'=>$marketer,'updated_at'=>date('Y-m-d H:i:s')];
                
                $result = $this->coupans_model->editCoupanassign($coupansInfo, $coupansId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Coupans Assign updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Coupans Assign updation failed');
                }
                
                redirect('coupansassign/coupansassignListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using coupansid
     * @return boolean $result : TRUE / FALSE
     */
    function deletecoupansassign()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $coupanId = $this->input->post('coupan_id');
           
            $coupanInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->coupans_model->deleteCoupan($coupanId, $coupanInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
}

?>