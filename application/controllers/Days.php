<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Days extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        
        parent::__construct();
        $this->load->model('days_model');
        $this->load->model('Month_model');
        $this->load->model('Subject_model');
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Subject list
     */
    public function daysListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('days_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->days_model->daysListingCount($searchText);
			$returns = $this->paginationCompress( "subjectListing/", $count, 5 );
            $data['daysRecords'] = $this->days_model->daysListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Days Listing';
            $this->loadViews("days/index", $this->global, $data, NULL);
        }
    }

    public function addNewday(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['month'] = $this->Month_model->getAllMonth();
            $this->global['pageTitle'] = 'SmartChildZone : Add New Day';
            $this->loadViews("days/create", $this->global,$data, NULL);
        }
    }


    
    /**
     * This function is used to add new Days to the system
     */
    function storeDays()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Subject Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('classes','Class','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewDay();
            }
            else
            {
                
                $name  = $this->input->post('name');
                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_days');

                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');
                $classes  = $this->input->post('classes');
                
                $dayInfo = ['month_id'=>$month, 'slug'=>$slug, 'sub_id'=>$subject,'class_id'=>$classes,'name'=>$name,'created_at'=>date('Y-m-d H:i:s')];
               
                $this->load->model('days_model');
                $result = $this->days_model->addNewDay($dayInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Day created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Days creation failed');
                }
                
                redirect('days/daysListing');
            }
        }
    }


    
    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function daysEdit($dayid = NULL)
    {
        
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($dayid == null)
            {
                redirect('days/daysListing');
            }
            
            $data['dayInfo'] = $this->days_model->getDayInfo($dayid);
            $data['months'] = $this->Month_model->getAllMonth();
            $data['subjects'] = $this->Subject_model->getAllSubject();
            $data['classes'] = $this->Subject_model->getAllClasses();
            $this->global['pageTitle'] = 'SmartChildZone : Edit Days';
            
            $this->loadViews("days/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateDay()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $dayId = $this->input->post('dayId');
            $this->form_validation->set_rules('name','Subject Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('class','Subjects','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->daysEdit($dayId);
            }
            else
            {
                $name = $this->input->post('name');
                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');
                $classes  = $this->input->post('class');
                
                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_days');

                $dayInfo = array();
                
                $dayInfo = array('month_id'=>$month,'slug'=>$slug, 'sub_id'=>$subject,'class_id'=>$classes,'name'=>$name,'updated_at'=>date('Y-m-d H:i:s'));
                
                
                $result = $this->days_model->editDay($dayInfo, $dayId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Day updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Day updation failed');
                }
                
                redirect('days/daysListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using subjectid
     * @return boolean $result : TRUE / FALSE
     */
    function deleteDay()
    {
        
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $dayId = $this->input->post('day_id');
            
            $dayInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->days_model->deleteDay($dayId, $dayInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
}

?>