<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Customers extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        
        parent::__construct();
        $this->load->model('Customer_model');
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Coupans list
     */
    public function Listing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            
            $this->load->model('coupans_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Customer_model->ListingCount($searchText);
			$returns = $this->paginationCompress( "customers/listing/", $count, 5 );
            $data['customers'] = $this->Customer_model->Listing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Customers Listing';
            $this->loadViews("customers/index", $this->global, $data, NULL);
        }
    }

    public function addNewCoupans(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('Month_model');
            $data['month'] = $this->Month_model->getAllMonth();
            $this->global['pageTitle'] = 'SmartChildZone : Add New coupans';
            $this->loadViews("coupans/create", $this->global,$data, NULL);
        }
    }


    
    /**
     * This function is used to add new Coupans to the system
     */
    function storeCoupans()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('c_coupon_code','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('c_coupon_per','Coupan per','trim|required|numeric');
            $this->form_validation->set_rules('c_coupon_desc','Coupan Description','trim|required|max_length[300]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewCoupans();
            }
            else
            {
                
                $coupan_code  = $this->input->post('c_coupon_code');
                $coupan_per  = $this->input->post('c_coupon_per');
                $coupan_des  = $this->input->post('c_coupon_desc');
                $coupan_status = $this->input->post('status') ? '1' : '0';
                
                $coupansInfo = ['c_coupon_code'=> $coupan_code,'c_coupon_per'=>$coupan_per,'c_coupon_desc'=>$coupan_des,'c_coupon_timestamp'=>date('Y-m-d H:i:s'),'c_coupon_status' => $coupan_status];
               
                $this->load->model('coupans_model');
                $result = $this->coupans_model->addNewCoupans($coupansInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Coupans created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Coupans creation failed');
                }
                
                redirect('coupans/coupansListing');
            }
        }
    }


    
    /**
     * This function is used load coupans edit information
     * @param number $coupansid : Optional : This is coupans id
     */
    function coupansEdit($coupanid = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($coupanid == null)
            {
                redirect('coupansListing');
            }
            
            $data['coupansInfo'] = $this->coupans_model->getCoupanInfo($coupanid);
            
            $this->global['pageTitle'] = 'SmartChildZone : Edit Coupans';
            
            $this->loadViews("coupans/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateCoupans()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $coupansId = $this->input->post('coupansId');
            
            $this->form_validation->set_rules('c_coupon_code','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('c_coupon_per','Coupan per','trim|required|numeric');
            $this->form_validation->set_rules('c_coupon_desc','Coupan Description','trim|required|max_length[300]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->coupansEdit($coupansId);
            }
            else
            {
                $coupan_code  = $this->input->post('c_coupon_code');
                $coupan_per  = $this->input->post('c_coupon_per');
                $coupan_des  = $this->input->post('c_coupon_desc');
                $coupan_status = $this->input->post('status') ? '1' : '0';
                
                $coupansInfo = array();

                $coupansInfo = ['c_coupon_code'=> $coupan_code,'c_coupon_per'=>$coupan_per,'c_coupon_desc'=>$coupan_des,'c_coupon_timestamp'=>date('Y-m-d H:i:s'),'c_coupon_status' => $coupan_status];
               
                
                $result = $this->coupans_model->editCoupan($coupansInfo, $coupansId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Coupans updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Coupans updation failed');
                }
                
                redirect('coupansListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using coupansid
     * @return boolean $result : TRUE / FALSE
     */
    function deleteCoupans()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $coupanId = $this->input->post('coupan_id');
           
            $coupanInfo = array('isDeleted'=>1,'c_coupon_timestamp'=>date('Y-m-d H:i:s'));
            
            $result = $this->coupans_model->deleteCoupan($coupanId, $coupanInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
    /**
     * This function is used to get the Coupan Assign listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    public function assignListing(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Coupanassign_model->coupansassignListingCount($searchText);
			$returns = $this->paginationCompress( "coupansListing/", $count, 5 );
            $data['coupansassignRecords'] = $this->Coupanassign_model->coupansassignListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Coupans Listing';
            $this->loadViews("coupansassign/index", $this->global, $data, NULL);
        }
    }


    public function addNewCoupansassign(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['coupans'] = $this->coupans_model->getAllCoupans();
            $data['marketers'] = $this->Marketer_model->getMarkterlist();
            $this->global['pageTitle'] = 'SmartChildZone : Add New coupans Assign';
            $this->loadViews("coupansassign/create", $this->global,$data, NULL);
        }
    }

       /**
     * This function is used to add new Coupans to the system
     */
    function storeCoupansassign()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('coupon_id','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('marketer_id','Markter','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewCoupansassign();
            }
            else
            {
                
                $coupan_code  = $this->input->post('coupon_id');
                $marketer  = $this->input->post('marketer_id');
                
                $coupanassignInfo = ['coupan_id'=> $coupan_code,'mar_id'=>$marketer,'created_at'=>date('Y-m-d H:i:s')];
               
            
                $result = $this->Coupanassign_model->addNewAssignCoupans($coupanassignInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Coupans Assign To Markter created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Coupans Assign creation failed');
                }
                
                redirect('coupans/assignListing');
            }
        }
    }

    /**
     * This function is used load coupans edit information
     * @param number $coupansid : Optional : This is coupans id
     */
    function coupansassignEdit($ca_id = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($ca_id == null)
            {
                redirect('coupans/coupansListing');
            }
            
            $data['coupansassignInfo'] = $this->Coupanassign_model->getCoupanassignInfo($ca_id);
            $data['coupans'] = $this->coupans_model->getAllCoupans();
            $data['marketers'] = $this->Marketer_model->getMarkterlist();

            $this->global['pageTitle'] = 'SmartChildZone : Edit Coupans Assign';
            
            $this->loadViews("coupansassign/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateCoupansassign()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $assignId = $this->input->post('assignId');
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('coupon_id','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('marketer_id','Markter','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->coupansassignEdit($assignId);
            }
            else
            {
                $coupan_code  = $this->input->post('coupon_id');
                $marketer  = $this->input->post('marketer_id');
                $coupansInfo = array();
                $coupansInfo = ['coupan_id'=> $coupan_code,'mar_id'=>$marketer,'updated_at'=>date('Y-m-d H:i:s')];
                
                $result = $this->Coupanassign_model->editCoupanassign($coupansInfo, $assignId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Coupans Assign updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Coupans Assign updation failed');
                }
                
                redirect('coupans/assignListing');
            }
        }
    }
    

     /**
     * This function is used to load the Subject list
     */
    public function commissionListing()
    {
        print_r('hello');
        exit;
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            print_r(hello); exit;
            $this->load->model('classes_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->Classes_model->classesListingCount($searchText);
            $returns = $this->paginationCompress( "classesListing/", $count, 5 );
            $data['classesRecords'] = $this->Classes_model->classesListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Classes Listing';
            $this->loadViews("classes/index", $this->global, $data, NULL);
        }
    }

}

?>