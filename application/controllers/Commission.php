<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Commission extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('coupans_model');
        $this->load->model('commission_model');
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Subject list
     */
    public function commissionListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->commission_model->commissionListingCount($searchText);
			$returns = $this->paginationCompress("commission/commissionListing/", $count, 2 );
            $data['commissionRecords'] = $this->commission_model->commissionListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Commission Listing';
            $this->loadViews("commission/index", $this->global, $data, NULL);
        }
    }

    public function addNewcommission(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['coupans'] = $this->coupans_model->getAllCoupans();
            $this->global['pageTitle'] = 'SmartChildZone : Add New Subject';
            $this->loadViews("commission/create", $this->global,$data, NULL);
        }
    }


    
    /**
     * This function is used to add new Subjects to the system
     */
    function storecommission()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('coupancode','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('commission','Commission','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewcommission();
            }
            else
            {
                
                $coupan  = $this->input->post('coupancode');
                $commission  = $this->input->post('commission');
                
                $commissionInfo = ['coupan_id'=> $coupan,'commission'=>$commission,'created_at'=>date('Y-m-d H:i:s')];
                
                $result = $this->commission_model->addNewcommission($commissionInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Commission created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Commission creation failed');
                }
                
                redirect('commission/commissionListing');
            }
        }
    }


    
    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function commissionEdit($commissionid = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($commissionid == null)
            {
                redirect('commission/commissionListing');
            }
           
            $data['coupans'] = $this->coupans_model->getAllCoupans();
            $data['commissioninfo'] = $this->commission_model->getCommissionInfo($commissionid);
            
            $this->global['pageTitle'] = 'SmartChildZone : Edit Commission';
            
            $this->loadViews("commission/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateCommission()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $commissionid = $this->input->post('commissionid');
            
            $this->form_validation->set_rules('coupan','Coupan Code','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('commission','Commission','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->commissionEdit($commissionid);
            }
            else
            {
                $coupan  = $this->input->post('coupan');
                $commission  = $this->input->post('commission');
                
                $c_Info = array();
                
                $c_Info = array('coupan_id'=>$coupan,'commission'=>$commission,'updated_at'=>date('Y-m-d H:i:s'));
                
                
                $result = $this->commission_model->editCommission($c_Info, $commissionid);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Commission updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Commission updation failed');
                }
                
                redirect('commission/commissionListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using subjectid
     * @return boolean $result : TRUE / FALSE
     */
    function deletecommission()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $subjectId = $this->input->post('sub_id');
           
            $subjectInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->subject_model->deleteSubject($subjectId, $subjectInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
}

?>