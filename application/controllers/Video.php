<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Video extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Classes_model');
        $this->load->model('Video_model');
        $this->load->model('Subject_model');
        $this->load->library('pagination');
        $this->isLoggedIn();   
    }

    public function index()
    {
        $this->isLoggedIn();
    }
    
    
    
    /**
     * This function is used to load the Subject list
     */
    public function videoListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $searchText = $this->input->post('email');
            $data['searchText'] = $searchText;
            $count = $this->Video_model->videoListingCount($searchText);
            $returns = $this->paginationCompress( "classesListing/", $count, 5 );
            $data['videos'] = $this->Video_model->videoListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Video Listing';
            $this->loadViews("video/index", $this->global, $data, NULL);
        }
    }

    public function addNewVideo(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['subjects'] = $this->Subject_model->getAllSubject();
            $this->global['pageTitle'] = 'SmartChildZone : Add New Class';
            $this->loadViews("video/create", $this->global, $data);
        }
    }

    /**
     * This function is used to add new Classes to the system
     */
    function storeVideo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Class Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('class','Class','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('topic','Topic','trim|required|numeric');
            if($this->form_validation->run() == FALSE){
                $this->addNewClasses();
            }else{
                
                if(isset($_FILES['video']['name']) && $_FILES['video']['name']!=""){
                    $configVideo['upload_path'] = 'assets/video'; # check path is correct
                    $configVideo['max_size'] = '102400';
                    $configVideo['allowed_types'] = 'mp4'; # add video extenstion on here
                    //$configVideo['overwrite'] = FALSE;
                    // $configVideo['remove_spaces'] = TRUE;
                    $video_name = time();
                    $configVideo['file_name'] = $video_name;
                    $this->load->library('upload', $configVideo);
                    $this->upload->initialize($configVideo);
                    //print_r($_FILES['video']['name']);exit;
                    if (!$this->upload->do_upload('video')){
                        return $this->upload->display_errors();
                        $this->session->set_flashdata('error', 'Video Upload failed.');
                    }else{
                        # Upload Successfull
                        $url = 'assets/video/'.$video_name.'.mp4';
                        //$set1 =  $this->Model_name->uploadData($url);
                        //$this->session->set_flashdata('success', 'Video Has been Uploaded');
                        //redirect('addNewClasses');
                    }
                }
                $name  = $this->input->post('name');
                $class  = $this->input->post('class');
                $subject  = $this->input->post('subject');
                $topic  = $this->input->post('topic');
                $youtube_video  = $this->input->post('youtube_video');
                $video_url = $url;

                $videoInfo = ['name'=> $name, 'class_id'=> $class, 'topic_id'=>$topic, 'embeded_code'=>$youtube_video, 'video_url'=>$video_url, 'subject_id'=>$subject,'created_at'=>date('Y-m-d H:i:s')];
                
                $result = $this->Video_model->addNewVideo($videoInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Classes created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Classes creation failed');
                }
                
                redirect('videoListing');
            }
        }
    }

    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function editVideo($video_id = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($video_id == null)
            {
                redirect('classesListing');
            }
            
            $data['videoInfo'] = $this->Video_model->getVideoInfo($video_id);

            $data['subjects'] = $this->Subject_model->getAllSubject();
            $data['classes'] = $this->Subject_model->getAllClasses();
            $data['topics'] = $this->Subject_model->getAllTopic();

            $this->global['pageTitle'] = 'SmartChildZone : Edit Classes';
            
            $this->loadViews("video/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateVideo()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $video_id = $this->input->post('video_id');
            
            
            $this->form_validation->set_rules('name','Class Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('class','Class','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('topic','Topic','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editVideo($video_id);
            }
            else
            {
                if(isset($_FILES['video']['name']) && $_FILES['video']['name']!=""){
                    $configVideo['upload_path'] = 'assets/video'; # check path is correct
                    $configVideo['max_size'] = '102400';
                    $configVideo['allowed_types'] = 'mp4'; # add video extenstion on here
                    //$configVideo['overwrite'] = FALSE;
                    // $configVideo['remove_spaces'] = TRUE;
                    $video_name = time();
                    $configVideo['file_name'] = $video_name;
                    $this->load->library('upload', $configVideo);
                    $this->upload->initialize($configVideo);
                    //print_r($_FILES['video']['name']);exit;
                    if (!$this->upload->do_upload('video')){
                        return $this->upload->display_errors();
                        $this->session->set_flashdata('error', 'Video Upload failed.');
                    }else{
                        # Upload Successfull
                        $url = 'assets/video/'.$video_name.'.mp4';
                        //$set1 =  $this->Model_name->uploadData($url);
                        //$this->session->set_flashdata('success', 'Video Has been Uploaded');
                        //redirect('addNewClasses');
                    }
                }

                $name  = $this->input->post('name');
                $class  = $this->input->post('class');
                $subject  = $this->input->post('subject');
                $topic  = $this->input->post('topic');
                $youtube_video  = $this->input->post('youtube_video');

                $video_url = $this->input->post('edit_video');
                if(isset($url) && !empty($url)){
                    $video_url = $url;
                }
                

                $videoInfo = ['name'=> $name, 'class_id'=> $class, 'topic_id'=>$topic, 'embeded_code'=>$youtube_video, 'video_url'=>$video_url, 'subject_id'=>$subject,'created_at'=>date('Y-m-d H:i:s')];
                
                
                $result = $this->Video_model->editVideo($videoInfo, $video_id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Classes updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Classes updation failed');
                }
                
                redirect('videoListing');
            }
        }
    }
    


   
}

?>