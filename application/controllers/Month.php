<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Month extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        
        parent::__construct();
        $this->load->model('month_model');
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Subject list
     */
    public function monthListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('month_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->month_model->monthListingCount($searchText);
			$returns = $this->paginationCompress( "subjectListing/", $count, 5 );
            $data['monthRecords'] = $this->month_model->monthListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Month Listing';
            $this->loadViews("month/index", $this->global, $data, NULL);
        }
    }

    public function addNewMonth(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = 'SmartChildZone : Add New Month';
            $this->loadViews("month/create", $this->global, NULL);
        }
    }


    
    /**
     * This function is used to add new months to the system
     */
    function storeMonth()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Subject Title','trim|required|max_length[128]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewMonth();
            }
            else
            {
                
                $name  = $this->input->post('name');

                $slug  = $this->create_unique_slug($name,'tbl_months');

                $monthInfo = ['name'=> $name,'slug'=>$slug,'created_at'=>date('Y-m-d H:i:s')];
                
                $this->load->model('month_model');
                $result = $this->month_model->addNewMonth($monthInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Month created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Month creation failed');
                }
                
                redirect('addNewMonth');
            }
        }
    }


    
    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function monthsEdit($monthid = NULL)
    {
        
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($monthid == null)
            {
                redirect('monthListing');
            }
            
            $data['monthInfo'] = $this->month_model->getMonthInfo($monthid);
            
            $this->global['pageTitle'] = 'SmartChildZone : Edit Month';
            
            $this->loadViews("month/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateMonth()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $monthId = $this->input->post('monthId');
            
            $this->form_validation->set_rules('name','Subject Title','trim|required|max_length[128]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->monthsEdit($monthId);
            }
            else
            {
                $name = $this->input->post('name');
                
                $slug  = $this->create_unique_slug($name,'tbl_months');

                $monthInfo = array();
                
                $monthInfo = array('name'=>$name,'slug'=>$slug,'updated_at'=>date('Y-m-d H:i:s'));
                
                $result = $this->month_model->editMonth($monthInfo, $monthId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Month updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Month updation failed');
                }
                
                redirect('month/monthListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using subjectid
     * @return boolean $result : TRUE / FALSE
     */
    function deleteMonth()
    {
        
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $monthId = $this->input->post('month_id');
            
            $monthInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->month_model->deleteMonth($monthId, $monthInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
}

?>