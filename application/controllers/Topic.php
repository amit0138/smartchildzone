<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Topic extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        
        parent::__construct();
        $this->load->model('topic_model');
        $this->load->model('Month_model');
        $this->load->model('Subject_model');
        $this->load->model('Classes_model');
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Subject list
     */
    public function topicsListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('topic_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->topic_model->topicListingCount($searchText);
			$returns = $this->paginationCompress( "topicsListing/", $count, 5 );
            $data['topicRecords'] = $this->topic_model->topicListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Topic Listing';
            $this->loadViews("topic/index", $this->global, $data, NULL);
        }
    }

    public function addNewTopic(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['month'] = $this->Month_model->getAllMonth();
            $this->global['pageTitle'] = 'SmartChildZone : Add New Topic';
            $this->loadViews("topic/create", $this->global,$data, NULL);
        }
    }


    
    /**
     * This function is used to add new Topics to the system
     */
    function storeTopic()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Topic','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('class','Class','trim|required|numeric');
            $this->form_validation->set_rules('days','Day','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewTopic();
            }
            else
            {
                
                $name  = $this->input->post('name');
                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');
                $classes  = $this->input->post('class');
                $days  = $this->input->post('days');

                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_topic');

                $topicInfo = ['name'=> $name,'slug'=>$slug, 'month_id'=>$month,'subject_id'=>$subject,'class_id'=>$classes,'day_id'=>$days, 'created_at'=>date('Y-m-d H:i:s')];
                
                $this->load->model('topic_model');
                $result = $this->topic_model->addNewTopic($topicInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Topic created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Topic creation failed');
                }
                
                redirect('topic/topicsListing');
            }
        }
    }


    
    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function TopicEdit($topicid = NULL)
    {
        
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($topicid == null)
            {
                redirect('topicsListing');
            }
            
            $data['topicInfo'] = $this->topic_model->getTopicInfo($topicid);
            $data['months'] = $this->Month_model->getAllMonth();
            $data['subjects'] = $this->Subject_model->getAllSubject();
            $data['classes'] = $this->Subject_model->getAllClasses();
            $data['days'] = $this->Subject_model->getAllDays();
            $this->global['pageTitle'] = 'SmartChildZone : Topic';
            
            $this->loadViews("topic/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateTopic()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $topicId = $this->input->post('topicId');
            
            $this->form_validation->set_rules('name','Topic','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('class','Class','trim|required|numeric');
            $this->form_validation->set_rules('days','Day','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->topicEdit($topicId);
            }
            else
            {
                $name  = $this->input->post('name');
                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');
                $classes  = $this->input->post('class');
                $days  = $this->input->post('days');

                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_topic');
                
                $topicInfo = array();
                
                $topicInfo = ['name'=> $name,'month_id'=>$month, 'slug'=>$slug, 'subject_id'=>$subject,'class_id'=>$classes,'day_id'=>$days, 'updated_at'=>date('Y-m-d H:i:s')];
                
                
                $result = $this->topic_model->editTopic($topicInfo, $topicId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Topic updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Topic updation failed');
                }
                
                redirect('topic/topicsListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using subjectid
     * @return boolean $result : TRUE / FALSE
     */
    function deleteTopic()
    {
        
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $topicId = $this->input->post('topic_id');
            
            $topicInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->topic_model->deleteTopic($topicId, $topicInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
}

?>