<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Subtopic extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        
        parent::__construct();
        $this->load->model('subtopic_model');
        $this->load->model('Month_model');
        $this->load->model('Subject_model');
        $this->isLoggedIn();   
    }
    
    
    /**
     * This function is used to load the Subject list
     */
    public function SubtopicListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('subtopic_model');
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            $this->load->library('pagination');
            $count = $this->subtopic_model->subtopicListingCount($searchText);
			$returns = $this->paginationCompress( "subjectListing/", $count, 5 );
            $data['subtopicRecords'] = $this->subtopic_model->subtopicListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Sub Topic Listing';
            $this->loadViews("subtopic/index", $this->global, $data, NULL);
        }
    }

    public function addNewSubtopic(){
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $data['month'] = $this->Month_model->getAllMonth();
            $this->global['pageTitle'] = 'SmartChildZone : Add New Sub Topic';
            $this->loadViews("subtopic/create", $this->global, $data);
        }
    }


    
    /**
     * This function is used to add new Topic to the system
     */
    function storeSubtopic()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name','Sub Topic Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('link','Link','trim|required|max_length[191]');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('class','Class','trim|required|numeric');
            $this->form_validation->set_rules('days','Day','trim|required|numeric');
            $this->form_validation->set_rules('topic','Topic','trim|required|numeric');
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewSubtopic();
            }
            else
            {
                
                $name  = $this->input->post('name');
                $link  = $this->input->post('link');
                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');
                $classes  = $this->input->post('class');
                $days  = $this->input->post('days');
                $topic  = $this->input->post('topic');

                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_subtopic');

                $subtopicInfo = ['name'=> $name,'link'=> $link, 'slug'=>$slug, 'month_id'=>$month,'subject_id'=>$subject,'class_id'=>$classes,'day_id'=>$days, 'topic_id'=>$topic, 'created_at'=>date('Y-m-d H:i:s')];
                
                $this->load->model('subtopic_model');
                $result = $this->subtopic_model->addNewSubtopic($subtopicInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Sub Topic created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Sub Topic creation failed');
                }
                
                redirect('subtopic/SubtopicListing');
            }
        }
    }


    
    /**
     * This function is used load Subject edit information
     * @param number $subjectid : Optional : This is subject id
     */
    function subtopicEdit($subtopicid = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($subtopicid == null)
            {
                redirect('subtopic/SubtopicListing');
            }
            
            $data['subtopicInfo'] = $this->subtopic_model->getSubtopicInfo($subtopicid);
            $data['months'] = $this->Month_model->getAllMonth();
            $data['subjects'] = $this->Subject_model->getAllSubject();
            $data['classes'] = $this->Subject_model->getAllClasses();
            $data['days'] = $this->Subject_model->getAllDays();
            $data['topics'] = $this->Subject_model->getAllTopic();

            $this->global['pageTitle'] = 'SmartChildZone : Edit Sub Topic';
            
            $this->loadViews("subtopic/edit", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to edit the user information
     */
    function updateSubtopic()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $subtopicId = $this->input->post('subtopicId');
            
            $this->form_validation->set_rules('name','Sub Topic Title','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('link','Link','trim|required|max_length[191]');
            $this->form_validation->set_rules('month','Month','trim|required|numeric');
            $this->form_validation->set_rules('subject','Subjects','trim|required|numeric');
            $this->form_validation->set_rules('class','Class','trim|required|numeric');
            $this->form_validation->set_rules('days','Day','trim|required|numeric');
            $this->form_validation->set_rules('topic','Topic','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->subtopicEdit($subtopicId);
            }
            else
            {
                $name  = $this->input->post('name');
                $link  = $this->input->post('link');
                $month  = $this->input->post('month');
                $subject  = $this->input->post('subject');
                $classes  = $this->input->post('class');
                $days  = $this->input->post('days');
                $topic  = $this->input->post('topic');

                //get slug from title
                $slug  = $this->create_unique_slug($name,'tbl_subtopic');
                
                $subtopicInfo = array();
                
                $subtopicInfo = ['name'=> $name,'link'=> $link, 'slug'=>$slug, 'month_id'=>$month,'subject_id'=>$subject,'class_id'=>$classes,'day_id'=>$days, 'topic_id'=>$topic, 'updated_at'=>date('Y-m-d H:i:s')];
                
                
                $result = $this->subtopic_model->editSubtopic($subtopicInfo, $subtopicId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Sub Topic updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Sub Topic updation failed');
                }
                
                redirect('subtopic/SubtopicListing');
            }
        }
    }
    
    /**
     * This function is used to delete the user using subjectid
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSubtopic()
    {
        
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $subtopicId = $this->input->post('subtopic_id');
            
            $subtopicInfo = array('isDeleted'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            
            $result = $this->subtopic_model->deleteSubtopic($subtopicId, $subtopicInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   
}

?>