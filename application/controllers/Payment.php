<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Payment extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Classes_model');
        $this->load->model('Payment_model');
        $this->load->model('Month_model');
        $this->load->library('pagination');
        $this->isLoggedIn();   
    }

    public function index()
    {
        $this->isLoggedIn();
    }
    
    
    
    /**
     * This function is used to load the Subject list
     */
    public function classesListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $searchText = $this->input->post('email');
            $data['searchText'] = $searchText;
            $count = $this->Payment_model->classesListingCount($searchText);
            $returns = $this->paginationCompress( "classesListing/", $count, 5 );
            $data['classesRecords'] = $this->Classes_model->classesListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'SmartChildZone : Classes Listing';
            $this->loadViews("classes/index", $this->global, $data, NULL);
        }
    }

    public function paymentListing(){
       
            $search_email = $this->input->post('searchemail');
            //print_r($search_email);exit;
            if(isset($search_email) && !empty($search_email)){
                $email = $search_email;
               
            }else{
                $email = $this->input->post('email')?$this->input->post('email'):null;
            }
            //print_r($search_email);exit;
            
            $searchText = $email;
            $data['searchText'] = $searchText;
            $data['paymentRecords'] = [];
            if(!empty($email)){
               
                $count = $this->Payment_model->classesPaymentCount($searchText,$email);
                $returns = $this->paginationCompress( "payment/paymentListing/", $count, 5 );
                //print_r($returns);exit;
                //$segment = $this->uri->segment ( SEGMENT );
                
                $data['paymentRecords'] = $this->Payment_model->paymentListing($searchText, $returns["page"], $returns["segment"],$email);

            }

            $this->global['pageTitle'] = 'Payment Listing';
            $this->loadViews("payment/index", $this->global, $data);
        
    }


   
}

?>