/**
 * @author Kishor Mali
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	jQuery(document).on("click", ".deleteSubject", function(){
		var sub_id = $(this).data("subjectid"),
			hitURL = baseURL + "subject/deleteSubject",
			currentRow = $(this);
		var confirmation = confirm("Are you sure to delete this Subject ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { sub_id : sub_id } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Subject successfully deleted"); }
				else if(data.status = false) { alert("Subject deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	jQuery(document).on("click", ".deleteMonth", function(){
		var month_id = $(this).data("monthid"),
			hitURL = baseURL + "month/deleteMonth",
			currentRow = $(this);
		var confirmation = confirm("Are you sure to delete this Month ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { month_id : month_id } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Month successfully deleted"); }
				else if(data.status = false) { alert("Month deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	jQuery(document).on("click", ".deleteCoupans", function(){
		var coupan_id = $(this).data("coupanid"),
			hitURL = baseURL + "coupans/deleteCoupans",
			currentRow = $(this);
		var confirmation = confirm("Are you sure to delete this Coupan ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { coupan_id : coupan_id } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Coupan successfully deleted"); }
				else if(data.status = false) { alert("Coupan deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	jQuery(document).on("click", ".searchList", function(){
		
	});
	
});
